<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'admin');
/*Route::controller('datatables', 'DatatablesController', [
    'anyData'  => 'datatables.data',
    'getIndex' => 'datatables',
]);*/



Route::group(['prefix' => 'admin'], function () {
    
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('getRetailerAjaxDataTable', 'Voyager\DashboardController@getRetailerAjaxDataTable');
    });

    Route::group(['prefix' => 'gis-company'], function () {
        Route::post('getDetails', 'Voyager\CompanyController@getDetails');
        Route::post('getMappings', 'Voyager\CompanyController@getMappings');
        Route::DELETE('companyMapDelete/{id}', 'Voyager\CompanyController@companyMapDelete');
    });

    Route::group(['prefix' => 'upc-review'], function () {
        Route::post('getUpcTitle', 'Voyager\UpcReviewController@getUpcTitle');
        Route::post('getTags', 'Voyager\UpcReviewController@getTags');
        Route::post('getUpc', 'Voyager\UpcReviewController@getUpc');
        Route::post('getRetailer', 'Voyager\UpcReviewController@getRetailer');
        Route::post('getUpcTitle', 'Voyager\UpcReviewController@getUpcTitle');
        Route::post('getTags', 'Voyager\UpcReviewController@getTags');
        Route::post('getUpc', 'Voyager\UpcReviewController@getUpc');
        Route::post('getRetailer', 'Voyager\UpcReviewController@getRetailer');
        /*add tags*/
        Route::post('addMnfTags', 'Voyager\UpcReviewController@addMnfTags');
        Route::post('addModelTags', 'Voyager\UpcReviewController@addModelTags');
        Route::post('addNameTags', 'Voyager\UpcReviewController@addNameTags');
        /*select2 dropdown list*/
        Route::post('mnfOverride', 'Voyager\UpcReviewController@mnfOverride');
        Route::post('modelOverride', 'Voyager\UpcReviewController@modelOverride');
        Route::post('nameOverride', 'Voyager\UpcReviewController@nameOverride');
        /*add data selected in select2*/
        Route::post('addMnfOverride', 'Voyager\UpcReviewController@addMnfOverride');
        Route::post('addModelOverride', 'Voyager\UpcReviewController@addModelOverride');
        Route::post('addNameOverride', 'Voyager\UpcReviewController@addNameOverride');
        
        Route::post('MnfDuplicationCheck', 'Voyager\UpcReviewController@MnfDuplicationCheck');
        Route::post('ModelDuplicationCheck', 'Voyager\UpcReviewController@ModelDuplicationCheck');
        Route::post('NameDuplicationCheck', 'Voyager\UpcReviewController@NameDuplicationCheck');
        /*Datatables*/
        Route::get('getOrgRetailerAjaxDataTable', 'Voyager\UpcReviewController@getOrgRetailerAjaxDataTable');
        Route::get('getItemdbRetailerAjaxDataTable', 'Voyager\UpcReviewController@getItemdbRetailerAjaxDataTable');
        Route::get('getAdminRetailerAjaxDataTable', 'Voyager\UpcReviewController@getAdminRetailerAjaxDataTable');
        Route::get('getUpcListAjaxDataTable', 'Voyager\UpcReviewController@getUpcListAjaxDataTable');
        /*update admin upc*/
        Route::post('updateAdmin', 'Voyager\UpcReviewController@updateAdmin');
    });


    Route::group(['prefix' => 'gs1-company'], function () {
        Route::post('getCompanyDetails', 'Voyager\Gs1CompanyController@getCompanyDetails');
        Route::post('updateGs1Company', 'Voyager\Gs1CompanyController@updateGs1Company');
        Route::post('getCompanyManufacturerMap', 'Voyager\Gs1CompanyController@getCompanyManufacturerMap');
        Route::post('getValidStatus', 'Voyager\Gs1CompanyController@getValidStatus');
        Route::post('deleteMapping', 'Voyager\Gs1CompanyController@deleteMapping');
        Route::get('getManufacturer', 'Voyager\Gs1CompanyController@getManufacturer');
        Route::post('addMapping', 'Voyager\Gs1CompanyController@addMapping');
        
        /*Datatables*/
        Route::get('getMappingAjaxDataTable', 'Voyager\Gs1CompanyController@getMappingAjaxDataTable');
        Route::get('getCompanyListAjaxDataTable', 'Voyager\Gs1CompanyController@getCompanyListAjaxDataTable');
    });


    Route::group(['prefix' => 'retailer'], function () {

        /*retailer list*/
        Route::get('getRetailerAjaxDataTable', 'Voyager\RetailerController@getRetailerAjaxDataTable');
        Route::get('getXpathDetails', 'Voyager\RetailerController@getXpathDetails');
        Route::post('updateRetailerXpath', 'Voyager\RetailerController@updateRetailerXpath');
        Route::post('editableData', 'Voyager\RetailerController@editableData');

        /*Cron table count*/
        Route::post('getCronTableCount', 'Voyager\RetailerController@getCronTableCount');
        /*Cron table details*/
        Route::post('CronTableDetails', 'Voyager\RetailerController@CronTableDetails');

        /*category*/
        Route::post('getCategory', 'Voyager\RetailerController@getCategory');
        Route::post('getCategoryDetails', 'Voyager\RetailerController@getCategoryDetails');

        /*product*/
        Route::post('getProduct', 'Voyager\RetailerController@getProduct');
        Route::post('getProductDetails', 'Voyager\RetailerController@getProductDetails');

        /*offer*/
        Route::post('getOffer', 'Voyager\RetailerController@getOffer');
        Route::post('getOfferDetails', 'Voyager\RetailerController@getOfferDetails');

        /*promotion*/
        Route::post('getPromotion', 'Voyager\RetailerController@getPromotion');
        Route::post('getPromotionDetails', 'Voyager\RetailerController@getPromotionDetails');

        /*server datatable*/
        Route::get('getXpathAjaxDataTable', 'Voyager\RetailerController@getXpathAjaxDataTable');
        Route::get('getProductAjaxDataTable', 'Voyager\RetailerController@getProductAjaxDataTable');
        Route::get('getCategoryAjaxDataTable', 'Voyager\RetailerController@getCategoryAjaxDataTable');
        Route::get('getOfferAjaxDataTable', 'Voyager\RetailerController@getOfferAjaxDataTable');
        Route::get('getPromotionAjaxDataTable', 'Voyager\RetailerController@getPromotionAjaxDataTable');

    });

    Voyager::routes();
});
