@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('css')
  <style type="text/css">
    .custom-title{
        font-size: 16px;
        padding-left: 60px;
        margin-bottom: -20px;
    }
    .custom-bordered>.custom-heading {
        border-bottom: 1px solid #e4eaec;
    }
    .custom-heading .custom-title{
        font-weight: 500;
        left:-10px;
    }
    .custom-heading{
        margin-top: -20px;
    }
    .custom-heading i{
        font-size: 22px;
    }
    .custom-body{
        left:0px;
    }
    table.dataTable thead>tr>th {
        padding-left: 5px;
    }
    table.dataTable tbody td, table.dataTable tbody th {
        padding: 8px 6px;
    }
    .dataTables_wrapper .row {
         margin-bottom: 0em; 
    }
    .get-details{
      cursor:pointer;
    }
    .map-btn{
      padding: 3px 6px;
    }
    .fixed-thead>tr>th{
      float:left;
    }
    .scroll-tbody{
      height:300px;
      overflow-y:auto;
      display:block;
    }
  </style>
@stop

@section('page_header')
  <div class="container-fluid">
    <h1 class="page-title">
      <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
    </h1>
    @can('add',app($dataType->model_name))
      <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
        <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
      </a>
    @endcan
    @can('delete',app($dataType->model_name))
      @include('voyager::partials.bulk-delete')
    @endcan
    @can('edit',app($dataType->model_name))
    @if(isset($dataType->order_column) && isset($dataType->order_display_column))
      <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
        <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
      </a>
    @endif
    @endcan
    @include('voyager::multilingual.language-selector')
  </div>
@stop


@section('content')
  <div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
      <div class="col-md-4">
        <div class="panel panel-bordered custom-bordered">
          <div class="panel-heading custom-heading">
            <h5 class="page-title custom-title">
              <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
            </h5>
          </div>
          <div class="panel-body custom-body">
            <div class="table-responsive">
              <table id="dataTable" class="table table-hover">
                <thead class="fixed-thead">
                  <tr>
                    @can('delete',app($dataType->model_name))
                      <th>
                        <input type="checkbox" class="select_all">
                      </th>
                    @endcan
                    @foreach($dataType->browseRows as $row)
                      @if(str_is('Manufacturer Name', $row->display_name))
                        <th>
                          {{ ucwords($row->display_name) }}
                        </th>
                      @endif
                    @endforeach
                  </tr>
                </thead>
                <tbody class="scroll-tbody">
                  @foreach($dataTypeContent as $data)
                  <tr>
                    @can('delete',app($dataType->model_name))
                      <td>
                        <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                      </td>
                    @endcan
                    @foreach($dataType->browseRows as $row)
                      @if(str_is('Manufacturer Name', $row->display_name))
                      <td>
                        <?php $options = json_decode($row->details); ?>
                        @if($row->type == 'text')
                          @include('voyager::multilingual.input-hidden-bread-browse')
                          <div class="readmore">
                            <a class="get-details" data-url="{{ url('admin/manufacturers/getDetails') }}" data-id="{{$data->manufacturer_id}}" title="View Details" >
                              {{ strtoupper(mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field})  }}
                            </a>
                          </div>
                        @endif
                      </td>
                      @endif
                    @endforeach
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-8">
        <!-- Company Details Start -->
        <span class="manufacturer-details"></span>
        <!-- Company Details Ends -->

        <!-- Company Mapping Start -->
        <span class="company-manufacturer-mapping"></span>
        <!-- Company Mapping Ends -->

        <!-- retailer Mapping Start -->
        <span class="retailer-manufacturer-mapping"></span>
        <!-- retailer Mapping Ends -->
      </div>
    </div>
  </div>

  {{-- Single delete modal --}}
  <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                        aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
        </div>
        <div class="modal-footer">
          <form action="#" id="delete_form" method="POST">
            {{ method_field("DELETE") }}
            {{ csrf_field() }}
            <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
          </form>
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@stop

@section('css')
@if(config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">

@endif
@stop

@section('javascript')
  {{-- Page Level JS --}}
  <script type="text/javascript" src="{{ asset('js/custom/manufacturer/manufacturer.js') }}"></script>
  <!-- DataTables -->
  @if(config('dashboard.data_tables.responsive'))
      <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
  @endif
  <script>
    $(document).ready(function () {
      var table = $('#dataTable').DataTable({
        paging: false,
        ordering: false,
        language: {
            //search: "_INPUT_",
            searchPlaceholder: "Search manufacturer"
        },
      });

      @if ($isModelTranslatable)
        $('.side-body').multilingual();
        //Reinitialise the multilingual features when they change tab
        $('#dataTable').on('draw.dt', function(){
            $('.side-body').data('multilingual').init();
        })
      @endif
      $('.select_all').on('click', function(e) {
        $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
      });
    });


    var deleteFormAction;
    $('td').on('click', '.delete', function (e) {
      $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
      $('#delete_modal').modal('show');
    });

  </script>
@stop
