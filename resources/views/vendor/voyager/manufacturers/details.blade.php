<div class="panel panel-bordered">
  <div class="panel-heading custom-heading">
    <h5 class="page-title custom-title">
      <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_singular.' Details' }}
    </h5>
  </div>

  <!-- form start -->
  <form role="form" class="form-edit-add" action="{{ route('voyager.'.$dataType->slug.'.update', $id) }}" method="POST" enctype="multipart/form-data">
    <!-- PUT Method if we are editing -->
    {{ method_field("PUT") }}

    <!-- CSRF TOKEN -->
    {{ csrf_field() }}

    <!-- panel-body -->
    <div class="panel-body">

      @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

      <!-- Editing -->
      @php
          $dataTypeRows = $dataType->{('editRows')};
      @endphp

      @foreach($dataTypeRows as $row)
        <!-- GET THE DISPLAY OPTIONS -->
        @php
            $options = json_decode($row->details);
            $display_options = isset($options->display) ? $options->display : NULL;
        @endphp
        @if ($options && isset($options->legend) && isset($options->legend->text))
            <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
        @endif
        @if ($options && isset($options->formfields_custom))
            @include('voyager::formfields.custom.' . $options->formfields_custom)
        @else 
          <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
            {{ $row->slugify }}
            <label for="name">{{ $row->display_name }}</label>
            @include('voyager::multilingual.input-hidden-bread-edit-add')
            @if($row->type == 'relationship')
              @include('voyager::formfields.relationship')
            @else
              {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
            @endif

            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
              {!! $after->handle($row, $dataType, $dataTypeContent) !!}
            @endforeach
          </div>
        @endif
      @endforeach
    </div>
    <!-- panel-body -->

    <div class="panel-footer text-right">
      @can('edit',app($dataType->model_name))
      	<button type="submit" class="btn btn-primary save">Update</button>
    	@endcan
    </div>
  </form>
</div>