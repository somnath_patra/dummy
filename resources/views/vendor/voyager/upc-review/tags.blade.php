<div class="panel panel-bordered custom-bordered row" style="padding-bottom:0px">
  <style type="text/css">
    .compact>tbody>tr>td, .compact>tbody>tr>th, .compact>tfoot>tr>td, .compact>tfoot>tr>th, .compact>thead>tr>td, .compact>thead>tr>th {
        padding: 0px 8px;
    }
    .compact{
      overflow: hidden;
    }
    .select2-container--default .select2-search--dropdown .select2-search__field {
      border-radius: 5px;
      border-color: #d3d8db;
    }
    .select2-container--open .select2-dropdown--below {
      border: 1px solid #d3d8db;
    }
    .select2-container .select2-selection--single {
      height: 34px;
      width: 100%;
      border: 1px solid #d3d8db;
      border-radius: 5px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
      top:4px;
    }
  </style>
  <div class="panel-heading accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1" aria-expanded="false" aria-controls="accordion1_1">
    <h3 class="panel-title" style="padding-top: 5px;padding-bottom: 30px;">
      <i class="fa fa-tag"></i> Tags
    </h3>
  </div>
  <div id="accordion1_1" class="panel-collapse collapse in" style="padding-bottom:5px">
    {{-- <div class="row" style="padding:10px 15px">
      <input id="demo" type="text" placeholder="Add tag">
    </div> --}}

    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover compact" style="margin-bottom:0px">
        <thead>
          <tr>
            <th style="padding-top:5px;padding-bottom:5px;" colspan="2">{{ ucwords('name') }}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="border-right:1px solid #d3d8db;padding:5px;" colspan="2" id="pre-name-tag">
              <div class="taginput-wrapper form-control" style="overflow-y:auto;max-height:90px;border:none;">
                @php $row = 'name'; @endphp
                @php $data = $tags->select($row)->get();$i=0; @endphp
                @if(count($data))
                  @foreach($data as $tag)
                    @if($tag->{$row})
                      <span class="tm-tag tm-tag-mini tm-tag-info">
                        <span>{{ $tag->{$row} }}</span> 
                      </span>
                      @php $i=1; @endphp
                    @endif
                  @endforeach
                @endif
                @if($i==0)
                  <span><i class="fa fa-info-circle"></i> No records found.</span> 
                @endif
              </div>
            </td>
          </tr>
          <tr>
            <td style="width:50%;border-right: 1px solid #d3d8db;" id="name-append">
              <div class="row" style="padding:10px 15px">
                <div class="taginput-wrapper form-control" style="overflow-y:auto;max-height:90px;">
                  <input type="text" name="tags" placeholder="Add Tags..." class="tm-input2" value="{{ isset($append_tags->admin_name_tags_append)?$append_tags->admin_name_tags_append:'' }}"/>
                </div>
              </div>
            </td>
            <td style="width:50%">
              <div class="row" style="padding:4px 15px">
                <select class="js-example-basic-single3" name="model">
                </select>
                <input type="hidden" id="name-override" value="{{ isset($append_tags->admin_name_override)?$append_tags->admin_name_override:'' }}">
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover compact" style="margin-bottom:0px">
        <thead>
          <tr>
            <th style="width: 15%;padding-top:5px;padding-bottom:5px;    border-right: 1px solid #d3d8db;">Fields</th>
            <th style="padding-top:5px;padding-bottom:5px">Tags</th>
          </tr>
        </thead>
        <tbody>
        @foreach($dataFields as $row)
        @if(str_contains($row, ['category_path','regular_price']))
          <tr>
            <td style="border-right: 1px solid #d3d8db;">{{ ucwords($row) }}</td>
            <td>
              <div class="taginput-wrapper form-control" style="overflow-y:auto;max-height:90px;border:none;">
                  @php $data = $tags->select($row)->get() @endphp
                  @if(count($data))
                    @foreach($data as $tag)
                      @if($tag->{$row})
                        <span class="tm-tag tm-tag-mini tm-tag-info">
                          <span>{{ $tag->{$row} }}</span> 
                        </span>
                      @endif
                    @endforeach
                  @else
                    <span><i class="fa fa-info-circle"></i> No records found.</span> 
                  @endif
              </div>
            </td>
          </tr>
        @endif
        @endforeach
        </tbody>
      </table>
    </div>

    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:5px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover compact" style="margin-bottom:0px">
        <thead>
          <tr>
            @foreach($dataFields as $row)
              @if(str_contains($row, ['manufacturer','model_number']))
                <th style="width:50%;padding-top:5px;padding-bottom:5px;">{{ ucwords($row) }}</th>
              @endif
            @endforeach
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="border-right:1px solid #d3d8db;padding: 5px" id="pre-mnf-tag">
              <div class="taginput-wrapper form-control" style="overflow-y:auto;max-height:90px;border:none;">
                @php $row = 'manufacturer'; @endphp
                @php $data = $tags->select($row)->get();$i=0; @endphp
                @if(count($data))
                  @foreach($data as $tag)
                    @if($tag->{$row})
                      <span class="tm-tag tm-tag-mini tm-tag-info">
                        <span>{{ $tag->{$row} }}</span> 
                      </span>
                      @php $i=1; @endphp
                    @endif
                  @endforeach
                @endif
                @if($i==0)
                  <span><i class="fa fa-info-circle"></i> No records found.</span> 
                @endif
              </div>
            </td>
            <td style="padding:5px" id="pre-model-tag">
              <div class="taginput-wrapper form-control" style="overflow-y:auto;max-height:90px;border:none;">
                @php $row = 'model_number'; @endphp
                @php $data = $tags->select($row)->get();$i=0; @endphp
                @if(count($data))
                  @foreach($data as $tag)
                    @if($tag->{$row})
                      <span class="tm-tag tm-tag-mini tm-tag-info">
                        <span>{{ $tag->{$row} }}</span> 
                      </span>
                      @php $i=1; @endphp
                    @endif
                  @endforeach
                @endif
                @if($i==0)
                  <span><i class="fa fa-info-circle"></i> No records found.</span> 
                @endif
              </div>
            </td>
          </tr>
          <tr>
            <td style="border-right: 1px solid #d3d8db;" id="mnf-append">
              <div class="row" style="padding:10px 15px">
                <div class="taginput-wrapper form-control" style="overflow-y:auto;max-height:90px;">
                  <input type="text" name="tags" placeholder="Add Tags..." class="tm-input" value="{{ isset($append_tags->admin_manufacturer_tags_append)?$append_tags->admin_manufacturer_tags_append:'' }}" />
                </div>
              </div>
            </td>
            <td id="model-append">
              <div class="row" style="padding:10px 15px">
                <div class="taginput-wrapper form-control" style="overflow-y:auto;max-height:90px;">
                  <input type="text" name="tags" placeholder="Add Tags..." class="tm-input1" value="{{ isset($append_tags->admin_model_tags_append)?$append_tags->admin_model_tags_append:'' }}" />
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td style="border-right: 1px solid #d3d8db;">
              <div class="row" style="padding:10px 15px">
                <select class="js-example-basic-single1" name="mnf">
                </select>
                <input type="hidden" id="mnf-override" value="{{ isset($append_tags->admin_manufacturer_override)?$append_tags->admin_manufacturer_override:'' }}">
              </div>
            </td>
            <td>
              <div class="row" style="padding:10px 15px">
                <select class="js-example-basic-single2" name="model">
                </select>
                <input type="hidden" id="model-override" value="{{ isset($append_tags->admin_model_number_override)?$append_tags->admin_model_number_override:'' }}">
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

  </div>
  <script type="text/javascript">

    {{-- manufacturer tags append --}}
    var addMnf = jQuery(".tm-input").tagsManager({
      prefilled: convertObj($(".tm-input").val()),
      tagClass: 'tm-tag-mini tm-tag-info',
      AjaxPush: '{!! url('admin/upc-review/addMnfTags') !!}',
      AjaxPushAllTags: true,
      AjaxPushParameters: { 'upc': $('.upc-title').html() },
    });
    /*validation*/
    jQuery(".tm-input").on('tm:pushing', function(e, tag) {
      $.ajax({
        url: '{!! url('admin/upc-review/MnfDuplicationCheck') !!}',
        type: 'POST',
        data: { 'upc' : $('.upc-title').html(), 'tag' : tag },
        success: function (response) {
          if(response==1)
          {
            $(".tm-input").tagsManager('popTag');
            return false;
          }
          else
          {
            addMnf.tagsManager();
          }
        },
        cache: true
      });
    });

    /* Model tags append */
    var addModel = jQuery(".tm-input1").tagsManager({
      prefilled: convertObj($(".tm-input1").val()),
      tagClass: 'tm-tag-mini tm-tag-info',
      AjaxPush: '{!! url('admin/upc-review/addModelTags') !!}',
      AjaxPushAllTags: true,
      AjaxPushParameters: { 'upc': $('.upc-title').html() }
    });
    /*validation*/
    jQuery(".tm-input1").on('tm:pushing', function(e, tag) {
      $.ajax({
        url: '{!! url('admin/upc-review/ModelDuplicationCheck') !!}',
        type: 'POST',
        data: { 'upc' : $('.upc-title').html(), 'tag' : tag },
        success: function (response) {
          if(response==1)
          {
            $(".tm-input1").tagsManager('popTag');
            return false;
          }
          else
          {
            addModel.tagsManager();
          }
        },
        cache: true
      });
    });

    /* Name tags append */
    var addName = jQuery(".tm-input2").tagsManager({
      prefilled: convertObj($(".tm-input2").val()),
      tagClass: 'tm-tag-mini tm-tag-info',
      AjaxPush: '{!! url('admin/upc-review/addNameTags') !!}',
      AjaxPushAllTags: true,
      AjaxPushParameters: { 'upc': $('.upc-title').html() }
    });
    /*validation*/
    jQuery(".tm-input2").on('tm:pushing', function(e, tag) {
      $.ajax({
        url: '{!! url('admin/upc-review/NameDuplicationCheck') !!}',
        type: 'POST',
        data: { 'upc' : $('.upc-title').html(), 'tag' : tag },
        success: function (response) {
          if(response==1)
          {
            $(".tm-input2").tagsManager('popTag');
            return false;
          }
          else
          {
            addName.tagsManager();
          }
        },
        cache: true
      });
    });

    /*convert json data to array*/
    function convertObj(obj)
    {
      if(!$.isEmptyObject(obj))
      {
        var data = $.parseJSON(obj);
        var arr = [];
        $.each(data, function (i,v)
        {
          arr.push(v);
        });
        return arr;
      }
      else
      {
        return '';
      }
    }

    var tags = {
      disableSelectedTag: function(id=''){
        if(id != '')
        {
          upcReview.selected_tags(id);
        }
        else
        {
          var id = ["mnf", "model", "name"];
          for (var i = 0; i < id.length; i++) {
             upcReview.selected_tags(id[i]);
          }
        }
      }
    }

    $(document).ready(function() {
      
      $('.js-example-basic-single1').select2({
        /*theme: "classic",*/
        placeholder: 'Select',
        ajax: {
          url: '{!! url('admin/upc-review/mnfOverride') !!}',
          type: 'POST',
          dataType: 'json',
          data: { 'upc' : $('.upc-title').html() },
          delay: 250,
          processResults: function (response) {
            return {
              results: response
            };
          },
          cache: true
        }
      });

      $('.js-example-basic-single2').select2({
        placeholder: 'Select',
        ajax: {
          url: '{!! url('admin/upc-review/modelOverride') !!}',
          type: 'POST',
          dataType: 'json',
          data: { 'upc' : $('.upc-title').html() },
          delay: 250,
          processResults: function (response) {
            return {
              results: response
            };
          },
          cache: true
        }
      });
      $('.js-example-basic-single3').select2({
        placeholder: 'Select',
        ajax: {
          url: '{!! url('admin/upc-review/nameOverride') !!}',
          type: 'POST',
          dataType: 'json',
          data: { 'upc' : $('.upc-title').html() },
          delay: 250,
          processResults: function (response) {
            return {
              results: response
            };
          },
          cache: true
        }
      });

      $('.js-example-basic-single1').on("change", function(e) { 
         var mnfOverride = $(".js-example-basic-single1 option:selected").text();
         $.ajax({
          url: '{!! url('admin/upc-review/addMnfOverride') !!}',
          type: 'POST',
          data: { 'upc' : $('.upc-title').html(), 'mnfOverride' : mnfOverride },
          success: function (response) {
            $('#mnf-override').val(mnfOverride);
            var id = 'mnf'
            tags.disableSelectedTag(id);
          },
          cache: true
         });
      });

      $('.js-example-basic-single2').on("change", function(e) { 
         var modelOverride = $(".js-example-basic-single2 option:selected").text();
         $.ajax({
          url: '{!! url('admin/upc-review/addModelOverride') !!}',
          type: 'POST',
          data: { 'upc' : $('.upc-title').html(), 'modelOverride' : modelOverride },
          success: function (response) {
            $('#model-override').val(modelOverride);
            var id = 'model'
            tags.disableSelectedTag(id);
          },
          cache: true
         });
      });

      $('.js-example-basic-single3').on("change", function(e) { 
         var nameOverride = $(".js-example-basic-single3 option:selected").text();
         $.ajax({
          url: '{!! url('admin/upc-review/addNameOverride') !!}',
          type: 'POST',
          data: { 'upc' : $('.upc-title').html(), 'nameOverride' : nameOverride },
          success: function (response) {
            $('#name-override').val(nameOverride);
            var id = 'name'
            tags.disableSelectedTag(id);
          },
          cache: true
         });
      });

      $('.select2').css('width','100%');

      tags.disableSelectedTag();
      
    });

  </script>
</div>