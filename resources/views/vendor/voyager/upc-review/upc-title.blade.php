<div class="panel panel-bordered custom-bordered row" style="padding-bottom:5px;padding-top: 25px;">
	<div class="panel-heading col-sm-5">
		<h3 class="panel-title" style="border-bottom:none;border-right:1px solid #ccc;margin-right: 0px">
		  <i class="fa fa-tag"></i> Final UPC
		</h3>
		<div class="pull-right" 
			 style="margin-top: -23px;padding-right: 10px">
		  <a class="upc-title" data-title="{{ $reviewUpcTitle->retailer_upc }}" style="color:#22A7F0;font-size:13px;font-weight:500">{{ $reviewUpcTitle->retailer_upc }}</a>
		</div>
	</div>

	<div class="panel-heading col-sm-7">
		<h3 class="panel-title" style="border-bottom:none;margin-left:5px">
		  <i class="fa fa-tag"></i> final product name
		</h3>
		<div class="pull-right" 
			 style="margin-top: -23px;padding-right: 0px">
		  <a class="" data-title="{{ $reviewUpcTitle->retailer_upc }}" style="color:#22A7F0;font-size:13px;font-weight:500">final product name</a>
		</div>
	</div>
</div>