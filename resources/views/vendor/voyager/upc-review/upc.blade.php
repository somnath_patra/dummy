<div class="panel panel-bordered custom-bordered row" style="padding-bottom:0px">
  <div class="panel-heading accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">
    <h3 class="panel-title" style="padding-top: 5px;padding-bottom: 30px;">
      <i class="fa fa-tag"></i> Review Summary
    </h3>
  </div>
  <div id="accordion1_2" class="panel-collapse collapse in">
    <div class="table-responsive" style="padding:0px;margin:10px 0px 15px 0px;border-radius:5px;">
      <table class="table table-hover table-striped table-bordered upc" style="margin-bottom:0px;">
        <thead>
            <tr>
              <th style="width:25%;vertical-align: middle;">#</th>
              <th style="vertical-align: middle;">Retailer</th>
              <th style="vertical-align: middle;">Itemdb</th>
              <th style="vertical-align: middle;">
                <span>Admin</span>
                <button class="btn btn-success pull-right" style="padding: 3px 6px;" title="Update" id="update-admin" onclick="upcReview.update_admin()"><i class="fa fa-save"></i></button></th>
            </tr>
        </thead>
        <tbody>
            <tr class="">
                <td>Upc</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_upc)
                      <a style="color: #22A7F0;
                                text-decoration: none;" 
                         href="#or-panel" id="main_retailer_upc"> 
                        {{ $upc->retailer_upc }} 
                      </a>
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_upc_derived)
                      <a style="color: #22A7F0;
                                text-decoration: none;" 
                         href="#itemdb-panel">
                        {{ $upc->itemdb_upc_derived }}
                       </a>
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_upc_override)
                      <input type="text" class="form-control admin-data" data-name="admin_upc_override" value="{{ $upc->admin_upc_override }}">
                    @else
                      <input type="text" class="form-control admin-data" data-name="admin_upc_override" value="{{ '-' }}">
                    @endif
                  @else
                    <input type="text" class="form-control admin-data" data-name="admin_upc_override" value="{{ '-' }}">
                  @endif
                </td>
            </tr>
            <tr class="">
                <td>{{ ucwords(str_replace('_',' ','_gs1_validated_flag')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_validated_flag)
                    {{ $upc->retailer_gs1_validated_flag }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_validated_flag)
                    {{ $upc->itemdb_gs1_validated_flag }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_validated_flag)
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_validated_flag" value="{{ $upc->admin_gs1_validated_flag }}">
                    @else
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_validated_flag" value="{{ '-' }}">
                    @endif
                  @else
                    <input type="text" class="form-control admin-data" data-name="admin_gs1_validated_flag" value="{{ '-' }}">
                  @endif
                </td>
            </tr>
            <tr class="">
                <td>{{ ucwords(str_replace('_',' ','_gs1_company_prefix')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_company_prefix)
                    {{ $upc->retailer_gs1_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_company_prefix)
                    {{ $upc->itemdb_gs1_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_company_prefix)
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_company_prefix" value="{{ $upc->admin_gs1_company_prefix }}">
                    @else
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_company_prefix" value="{{ '-' }}">
                    @endif
                  @else
                    <input type="text" class="form-control admin-data" data-name="admin_gs1_company_prefix" value="{{ '-' }}">
                  @endif
                </td>
            </tr>
            <tr class="">
                <td>{{ ucwords(str_replace('_',' ','gs1_upc_company_prefix')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_upc_company_prefix)
                    {{ $upc->retailer_gs1_upc_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_upc_company_prefix)
                    {{ $upc->itemdb_gs1_upc_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_upc_company_prefix)
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_upc_company_prefix" value="{{ $upc->admin_gs1_upc_company_prefix }}">
                    @else
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_upc_company_prefix" value="{{ '-' }}">
                    @endif
                  @else
                    <input type="text" class="form-control admin-data" data-name="admin_gs1_upc_company_prefix" value="{{ '-' }}">
                  @endif
                </td>
            </tr>
            <tr class="">
                <td>{{ ucwords(str_replace('_',' ','_gs1_last_update_date')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_last_update_date)
                    {{ $upc->retailer_gs1_last_update_date }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_last_update_date)
                    {{ $upc->itemdb_gs1_last_update_date }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_last_update_date)
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_last_update_date" value="{{ $upc->admin_gs1_last_update_date }}">
                    @else
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_last_update_date" value="{{ '-' }}">
                    @endif
                  @else
                    <input type="text" class="form-control admin-data" data-name="admin_gs1_last_update_date" value="{{ '-' }}">
                  @endif
                </td>
            </tr>
            <tr class="">
                <td>{{ ucwords(str_replace('_',' ','_gs1_analysis_log')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_analysis_log)
                    {{ $upc->retailer_gs1_analysis_log }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_analysis_log)
                    {{ $upc->itemdb_gs1_analysis_log }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_analysis_log)
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_analysis_log" value="{{ $upc->admin_gs1_analysis_log }}">
                    @else
                      <input type="text" class="form-control admin-data" data-name="admin_gs1_analysis_log" value="{{ '-' }}">
                    @endif
                  @else
                    <input type="text" class="form-control admin-data" data-name="admin_gs1_analysis_log" value="{{ '-' }}">
                  @endif
                </td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>