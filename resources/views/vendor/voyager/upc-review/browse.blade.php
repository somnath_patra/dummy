@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/page-css/upc-review.css') }}">
  <link rel="stylesheet" href="{{ asset('public/tag-manager/tagmanager.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/custom-dataTable.css') }}">
@stop

@section('page_header')
  <div class="container-fluid">
    <h1 class="page-title">
      <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
    </h1>
    @include('voyager::multilingual.language-selector')
    <div class="pull-right" style="margin-top:2px;">
      <button class="btn btn-info btn-md" title="Minimize All Panel" id="collapse-all"><i class="fa fa-eye"></i></button>
      <button class="btn btn-info btn-md" title="Expand All Panel" style="display: none" id="minimize-all"><i class="fa fa-eye-slash"></i></button>
    </div>
  </div>
@stop


@section('content')
  <div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
      <div class="col-md-2">
        <div class="panel panel-bordered custom-bordered" style="padding:20px 0px 0px 0px;">
          <div class="panel-heading" 
               style="padding: 0px 5px">
            <h3 class="panel-title" style="padding-top: 5px;padding-bottom: 30px;">
              <span id="list-title"><i class="fa fa-tag"></i> Upc List</span>
              <select id="dataTable-length" 
                style="padding: 3px;
                       border-radius: 15px;
                       border-color: #e4eaec;">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
              </select>
              <button type="button" id="reset" class="btn btn-xs reset-btn" ><i class="fa fa-undo"></i></button>
            </h3>
            <div  class="pull-right" 
                  style="margin-top: -35px;padding-right: 0px">
              <a id="browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1" style="color:#22A7F0;"></a>
            </div>
          </div>
            
          <div id="accordion2_1" class="panel-collapse collapse in">
            <div class="table-responsive">
              
              {{-- Custom filter --}}
              <div class="col-sm-12 col-md-12 col-xs-12">
                <table class="table filter-dataTable">
                  <thead>
                    <tr>
                      <td width="100%">
                        <input type="text" 
                             data-action="filter" 
                             data-filters="#dataTable" 
                             class="form-control" 
                             id="task-table-filter" 
                             placeholder="Search..."
                             style="height:28px;width:100%;">
                        <span class="glyphicon glyphicon-search form-control-feedback" style="color: #22A7F0;top:8px;"></span>
                      </td>
                    </tr>
                  </thead>
                </table>
              </div>
              {{-- Custom filter --}}

              {{-- main content --}}
              <table class="table table-hover" id="dataTable">
                <thead>
                  
                </thead>
                <tbody style="display:block;
                              max-height:350px;
                              overflow-y:auto;">
                  
                </tbody>
              </table>
              {{-- main content --}}

            </div>
          </div>
        </div>
      </div>

      <div class="col-md-10">
        <center id="custom-loader" style="display: none;">
          <img src="{{ asset('public/vendor/tcg/voyager/assets/loader/custom_loader.gif') }}" class="img-responsive" width="70px"/>
        </center>
        <div class="col-sm-12 get-upc-title">
        </div>
        <div class="panel-group" id="accordion1" aria-multiselectable="true">
          <div class="col-sm-12 get-tags"></div>
          <div class="col-sm-12 get-upc"></div>
          <div class="col-sm-12 get-retailer"></div>
        </div>
      </div>
    </div>
  </div>

@stop

@section('javascript')
  {{-- Page Level JS --}}
  <script type="text/javascript" src="{{ asset('public/js/custom/upc-review/upc-review.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/tag-manager/tagmanager.js') }}"></script>
  <script src="{{ asset('public/select2/select2.min.js') }}"></script>

  <!-- DataTables -->
  <script src="{{ asset('public/js/custom/upc-review/upc-dataTable.js') }}"></script>

@stop
