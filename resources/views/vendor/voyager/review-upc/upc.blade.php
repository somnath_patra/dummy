<div class="panel panel-bordered custom-bordered row" style="padding-bottom:0px">
  <div class="panel-heading accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">
    <h3 class="panel-title" style="padding-top: 5px;padding-bottom: 30px;">
      <i class="fa fa-tag"></i> Review Summary
    </h3>
  </div>
  <div id="accordion1_2" class="panel-collapse collapse in">
    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 15px 0px;border-radius:5px;">
      <table class="table table-hover table-striped upc" style="margin-bottom:0px;">
        <thead>
            <tr>
              <th style="width:25%;border-right: 1px solid #d3d8db;">Fields</th>
              <th>Retailer</th>
              <th>Itemdb</th>
              <th>Admin</th>
            </tr>
        </thead>
        <tbody>
            <tr class="">
                <td style="border-right: 1px solid #d3d8db;">Upc</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_upc)
                      <a style="color: #22A7F0;
                                text-decoration: none;" 
                         href="#or-panel"> 
                        {{ $upc->retailer_upc }} 
                      </a>
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_upc_derived)
                      <a style="color: #22A7F0;
                                text-decoration: none;" 
                         href="#itemdb-panel">
                        {{ $upc->itemdb_upc_derived }}
                       </a>
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_upc_override)
                      <a style="color: #22A7F0;
                                text-decoration: none;" 
                         href="#admin-panel"> 
                        {{ $upc->admin_upc_override }}
                      </a>
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
            </tr>
            <tr class="">
                <td style="border-right: 1px solid #d3d8db;">{{ ucwords(str_replace('_',' ','_gs1_validated_flag')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_validated_flag)
                    {{ $upc->retailer_gs1_validated_flag }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_validated_flag)
                    {{ $upc->itemdb_gs1_validated_flag }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_validated_flag)
                    {{ $upc->admin_gs1_validated_flag }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
            </tr>
            <tr class="">
                <td style="border-right: 1px solid #d3d8db;">{{ ucwords(str_replace('_',' ','_gs1_company_prefix')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_company_prefix)
                    {{ $upc->retailer_gs1_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_company_prefix)
                    {{ $upc->itemdb_gs1_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_company_prefix)
                    {{ $upc->admin_gs1_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
            </tr>
            <tr class="">
                <td style="border-right: 1px solid #d3d8db;">{{ ucwords(str_replace('_',' ','gs1_upc_company_prefix')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_upc_company_prefix)
                    {{ $upc->retailer_gs1_upc_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_upc_company_prefix)
                    {{ $upc->itemdb_gs1_upc_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_upc_company_prefix)
                    {{ $upc->admin_gs1_upc_company_prefix }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
            </tr>
            <tr class="">
                <td style="border-right: 1px solid #d3d8db;">{{ ucwords(str_replace('_',' ','_gs1_last_update_date')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_last_update_date)
                    {{ $upc->retailer_gs1_last_update_date }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_last_update_date)
                    {{ $upc->itemdb_gs1_last_update_date }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_last_update_date)
                    {{ $upc->admin_gs1_last_update_date }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
            </tr>
            <tr class="">
                <td style="border-right: 1px solid #d3d8db;">{{ ucwords(str_replace('_',' ','_gs1_analysis_log')) }}</td>
                <td>
                  @if(!empty($upc))
                    @if($upc->retailer_gs1_analysis_log)
                    {{ $upc->retailer_gs1_analysis_log }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->itemdb_gs1_analysis_log)
                    {{ $upc->itemdb_gs1_analysis_log }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
                <td>
                  @if(!empty($upc))
                    @if($upc->admin_gs1_analysis_log)
                    {{ $upc->admin_gs1_analysis_log }}
                    @else
                      {{ '-' }}
                    @endif
                  @else
                    {{ '-' }}
                  @endif
                </td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>