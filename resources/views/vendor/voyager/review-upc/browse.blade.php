@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/page-css/review-upc.css') }}">
  <link rel="stylesheet" href="{{ asset('public/tag-manager/tagmanager.css') }}">
@stop

@section('page_header')
  <div class="container-fluid">
    <h1 class="page-title">
      <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
    </h1>
    @include('voyager::multilingual.language-selector')
    <div class="pull-right" style="margin-top:2px;">
      <button class="btn btn-info btn-md" title="Minimize All Panel" id="collapse-all"><i class="fa fa-eye"></i></button>
      <button class="btn btn-info btn-md" title="Expand All Panel" style="display: none" id="minimize-all"><i class="fa fa-eye-slash"></i></button>
    </div>
  </div>
@stop


@section('content')
  <div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
      <div class="col-md-2">
        <div class="panel panel-bordered custom-bordered" style="padding:20px 0px 0px 0px;">
          <div class="panel-heading" 
               style="padding: 0px 5px">
            <h3 class="panel-title" style="padding-top: 5px;padding-bottom: 30px;">
              <span id="list-title"><i class="fa fa-tag"></i> Upc List</span>
              <select id="dataTable-length" 
                style="padding: 3px;
                       border-radius: 15px;
                       border-color: #e4eaec;">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
              </select>
            </h3>
            <div  class="pull-right" 
                  style="margin-top: -35px;padding-right: 0px">
              <a id="browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1" style="color:#22A7F0;"></a>
            </div>
          </div>
          {{-- <div class="panel-heading accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1" aria-expanded="false" aria-controls="accordion2_1" style="border-bottom:solid 1px #eee;">
            <div style="margin:5px 5px 0px 5px;padding-bottom:10px">
              <input type="text" data-action="filter" data-filters="#dataTable" class="form-control" id="task-table-filter" placeholder="Search...">
              <span class="glyphicon glyphicon-search form-control-feedback" style="color: #22A7F0"></span>
            </div>
          </div> --}}
          <div id="accordion2_1" class="panel-collapse collapse in" style="padding-bottom:5px">
            <div class="table-responsive">
              <style type="text/css">
                #dataTable thead th.sorting_asc:after{
                  display: none;
                }
                #dataTable_paginate .pagination li {
                  display: none;
                }
                #dataTable_paginate .pagination li.previous, #dataTable_paginate .pagination li.next
                {
                  display: inline;
                }
                .pagination{
                  padding-left: 6px;
                }
                .voyager div.dataTables_paginate li.previous a:before {
                  margin-left: -5px;
                }
                #dataTable_previous a{
                  padding-left: 13px;
                }
                #dataTable_next a{
                  padding-right: 15px;
                }
                
                div.dataTables_paginate li.first a:before, div.dataTables_paginate li.previous a:before {
                  top: 0px;
                }
                div.dataTables_paginate li.last a:after, div.dataTables_paginate li.next a:after {
                  top: 0px;
                }
                .select2-container {
                  margin-top: 6px;
                }
                .select2-container--default .select2-selection--multiple .select2-selection__choice {
                  border-radius: 3px;
                  margin-right: 5px;
                  margin-top: 3px;
                  padding: 0 3px;
                }
                .select2-container .select2-selection--multiple{
                  min-height: 28px;
                }
                .select2-container--default .select2-selection--multiple{
                  border-color: #e4eaec;
                }
                .select2-container--default.select2-container--focus .select2-selection--multiple{
                  border-color: #62a8ea;
                }
                .select2-container--default .select2-search--inline .select2-search__field{
                  padding-left: 5px;
                  color:#76838f !important;
                }
                @media (min-width: 768px)
                {
                  .form-inline .form-control {
                    width: 100%;
                  }
                }
              </style>
              <table class="table table-hover" id="dataTable">
                <thead style="display:table;
                              width:100%;
                              table-layout:fixed;">
                  <tr>
                    <th style="padding:5px;">
                      <input type="text" 
                             data-action="filter" 
                             data-filters="#dataTable" 
                             class="form-control" 
                             id="task-table-filter" 
                             placeholder="Search..."
                             style="height:28px;width:100%;">
                      <span class="glyphicon glyphicon-search form-control-feedback" style="color: #22A7F0;top:3px;"></span>
                                            <div class="taginput-wrapper form-control" style="margin-top:6px;padding: 0px 0px 0.4em 0px">
                        <div class="col-sm-12" 
                             style="background-color: #f5f5f5;padding:0.4em 0.2em 0.4em 0em;color:#555">
                          <div class="col-sm-10"> Custom Filter</div>
                          <div class="col-sm-2">
                            <button type="button" id="reset" class="btn btn-primary btn-xs" style="margin:0px;padding: 2px 5px;font-size:10px"><i class="fa fa-undo"></i></button>
                          </div>
                        </div>

                        <div class="" style="padding:0.2em 0.4em">
                          <div class="col-sm-12" style="margin-top:5px;">
                            Enabled Flag
                          </div>
                          <div class="col-sm-12">
                            <input class="dropdown-item" type="radio" name="flag_filter" data-id="enabled_flag" value="true" > True
                            <input class="dropdown-item" type="radio" name="flag_filter" data-id="enabled_flag" value="false"> False
                            <input class="dropdown-item" type="radio" name="flag_filter" data-id="enabled_flag" value="" checked> Any
                          </div>
                          <div class="col-sm-12" style="border-bottom:1px solid #eee;margin: 5px 0px;"></div>
                          <div class="col-sm-12">
                            Api Enabled
                          </div>
                          <div class="col-sm-12">
                            <input class="dropdown-item" type="radio" name="api_filter" data-id="api_enabled" value="true"> True
                            <input class="dropdown-item" type="radio" name="api_filter" data-id="api_enabled" value="false"> False
                            <input class="dropdown-item" type="radio" name="api_filter" data-id="api_enabled" value="" checked> Any
                          </div>
                          </div>
                        </div>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody style="display:block;
                              max-height:300px;
                              overflow-y:auto;">
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-10">
        <center id="custom-loader" style="display: none;">
          <img src="{{ asset('vendor/tcg/voyager/assets/loader/custom_loader.gif') }}" class="img-responsive" width="70px"/>
        </center>
        <div class="col-sm-12 get-upc-title">
        </div>
        <div class="panel-group" id="accordion1" aria-multiselectable="true">
          <div class="col-sm-12 get-tags"></div>
          <div class="col-sm-12 get-upc"></div>
          <div class="col-sm-12 get-retailer"></div>
        </div>
      </div>
    </div>
  </div>

  {{-- Single delete modal --}}
  <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                        aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} item?</h4>
        </div>
        <div class="modal-footer">
          {{-- <form action="#" id="delete_form" method="POST">
              {{ method_field("DELETE") }}
              {{ csrf_field() }} --}}
              <input type="button" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}" id="confirm_delete">
          {{-- </form> --}}
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@stop

@section('css')
@if(config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">

@endif
@stop

@section('javascript')
  {{-- Page Level JS --}}
  <script type="text/javascript" src="{{ asset('public/js/custom/review-upc/review-upc.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/tag-manager/tagmanager.js') }}"></script>
  <script src="{{ asset('public/select2/select2.min.js') }}"></script>

  <!-- DataTables -->
  @if(config('dashboard.data_tables.responsive'))
      <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
  @endif
  <script>
    $(document).ready(function () {
      /*Server side Datatable*/
      $(function() {
        $('#dataTable').DataTable({
          processing: false,
          serverSide: true,
          ajax: '{!! url('admin/review-upc/getUpcListAjaxDataTable') !!}',
          columns: [
              { data: 'retailer_upc', name: 'retailer_upc',
                "render": function(data,type,row,meta) {  
                    var a = '<a class="get-details" data-url="{{ url('admin/review-upc/getUpcTitle') }}" data-id="'+row.upc_review_id+'" title="View Details" onclick="reviewUpc.get_upc_title(this)">' + row.retailer_upc +'</a>'; 
                        return a;
                },
                orderable: false
              }
          ]
        });

        oTable = $('#dataTable').DataTable();   
        $('#task-table-filter').keyup(function(){
          oTable.search($(this).val()).draw();
        });

        $('#dataTable-length').val(oTable.page.len());
        $('#dataTable-length').change( function() { 
          oTable.page.len( $(this).val() ).draw();
        });

        $('#dataTable').css('width','100%');
        $("#dataTable_info").parent().closest('div').css('display', 'none');
        $("#dataTable_paginate").parent().closest('div').attr('class', 'col-sm-12');
        $("#dataTable_paginate").css('float','left');

      });
      
      /*Custom Filter*/
      $(".dropdown-item").on("change", function(){
        
        var value = '';
        var searchby = [];
        
        if($("input[name='flag_filter']").is(':checked'))
        {
          value = $("input[name='flag_filter']:checked").val();
          searchby.push({value:value, index:$("input[name='flag_filter']").attr("data-id")});
        }
        if($("input[name='api_filter']").is(':checked'))
        {
          value = $("input[name='api_filter']:checked").val();
          searchby.push({value:value, index:$("input[name='api_filter']").attr("data-id")});
        }
        
        var d = JSON.stringify(searchby);
        console.log(d);
        oTable.search(d).draw();
      });

      /*Reset filter*/
      $('#reset').on("click", function(){
        $("input[type='checkbox']").prop('checked',false);
        oTable.search('').draw();
      });

      /*hide/show html tags*/ 
      if($('#accordion2_1').hasClass('panel-collapse collapse in')) 
      {
        $('#list-title').css('display','none');
      }

      $('#browse').on("click", function(){
        if($('#browse').hasClass('accordion-toggle collapsed')) 
        {
          $('#dataTable-length').show();
          $('#list-title').hide();
        }
        else
        {
          $('#dataTable-length').hide();
          $('#list-title').show();
        }
      });

      $('#collapse-all').on("click", function(){
        $('#collapse-all').css('display', 'none')
        $('#minimize-all').show();
        $('.accordion-toggle').addClass('collapsed');
        $('.accordion-toggle').attr('aria-expanded','false');
        $('.panel-collapse').attr('aria-expanded','false');
        $('.panel-collapse').removeClass('in');

        $('#or-length').css('display','none');
        $('#itemdb-length').css('display','none');
        $('#admin-length').css('display','none');
        $('#or-search-panel').css('display','none');
        $('#itemdb-search-panel').css('display','none');
        $('#admin-search-panel').css('display','none');
      });

      $('#minimize-all').on("click", function(){
        $('#minimize-all').css('display', 'none')
        $('#collapse-all').show();
        $('.accordion-toggle').removeClass('collapsed');
        $('.accordion-toggle').attr('aria-expanded','true');
        $('.panel-collapse').attr('aria-expanded','true');
        $('.panel-collapse').addClass('in');

        $('#or-length').show();
        $('#itemdb-length').show();
        $('#admin-length').show();
        $('#or-search-panel').show();
        $('#itemdb-search-panel').show();
        $('#admin-search-panel').show();
      });

    });
  </script>
@stop
