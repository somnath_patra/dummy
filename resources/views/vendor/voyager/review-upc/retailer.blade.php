@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/search-box.css') }}">
@stop

{{-- ORIGINAL RETAILER --}}
<div class="panel panel-bordered custom-bordered row" id="or-panel" style="padding-bottom:0px;">
  <div class="panel-heading">
    <h3 class="panel-title" style="padding-top:5px;padding-bottom: 30px;">
      <i class="fa fa-tag"></i> Original Retailer (<span id="or-upc" style="color:#22A7F0;font-size:0.88em">{{ !empty($upc->retailer_upc)?$upc->retailer_upc:'' }}</span>)
      <div  class="pull-right" 
          style="margin-top: -0px;padding-right: 20px">
        
        <div class="or-search-form has-feedback" id="or-search-panel">
          <input type="text" data-action="filter" data-filters="#original-retailer" class="form-control" id="or-search" placeholder="Search..." style="border-radius:25px;height:27px">
          <span class="glyphicon glyphicon-search form-control-feedback" style="color:#22A7F0;line-height:29px"></span>
        </div>
      </div>
      <div  class="pull-right" 
            style="margin-top: -0px;padding-right: 8px">
        <select id="or-length" 
                style="padding: 3px;
                       border-radius: 15px;
                       border-color: #e4eaec;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </div>
    </h3>
    <div  class="pull-right" 
          style="margin-top: -35px;padding-right: 0px">
      <a id="or-browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3" style="color:#22A7F0;"></a>
    </div>
  </div>
  <div id="accordion1_3" class="panel-collapse collapse in">
    
    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover table-striped" id="original-retailer">
        <thead>
            <tr>
                <th>Name</th>
                <th>Model Number</th>
                {{-- <th>Product Name</th> --}}
                <th>Manufacturer</th>
                <th>Category Path</th>
                <th>Regular Price</th>
            </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
    </div>
  </div>
  <script type="text/javascript">
    $(function() {
      $('#original-retailer').DataTable({
        pageLength: 10,
        oLanguage: {
          sProcessing: "<center><img src='{{ asset('vendor/tcg/voyager/assets/loader/custom_loader.gif') }}' class='img-responsive' width='70px'/></center>"
        },
        processing: false,
        serverSide: true,
        ajax: '{!! url('admin/review-upc/getOrgRetailerAjaxDataTable?upc=') !!}'+$('#or-upc').html(),
        columns: [
            { data: 'name', name: 'name' },
            { data: 'model_number', name: 'model_number' },
            /*{ data: 'name', name: 'name' },*/
            { data: 'manufacturer', name: 'manufacturer' },
            { data: 'category_path', name: 'category_path' },
            { data: 'regular_price', name: 'regular_price' },
        ]
      });

      orTable = $('#original-retailer').DataTable();   
      $('#or-search').keyup(function(){
            orTable.search($(this).val()).draw() ;
      });

      $('#or-length').val(orTable.page.len());
      $('#or-length').change( function() { 
        orTable.page.len( $(this).val() ).draw();
      });

      $('#original-retailer').css('width', '100%');
    });
  </script>
</div>

{{-- ITEMDB RETAILER --}}
<div class="panel panel-bordered custom-bordered row" id="itemdb-panel" style="padding-bottom:0px;">
  <div class="panel-heading">
    <h3 class="panel-title" style="padding-top:5px;padding-bottom:30px;">
      <i class="fa fa-tag"></i> Itemdb Retailer (<span id="itemdb-upc" style="color:#22A7F0;font-size:0.88em">{{ !empty($upc->itemdb_upc_derived)?$upc->itemdb_upc_derived:'' }}</span>)
      <div  class="pull-right" 
          style="margin-top: 0px;padding-right: 20px">
        
        <div class="or-search-form has-feedback" id="itemdb-search-panel">
          <input type="text" data-action="filter" data-filters="#original-retailer" class="form-control" id="itemdb-search" placeholder="Search..." style="border-radius:25px;height:27px">
          <span class="glyphicon glyphicon-search form-control-feedback" style="color:#22A7F0;line-height:29px"></span>
        </div>
      </div>
      <div  class="pull-right" 
            style="margin-top: 0px;padding-right: 8px">
        <select id="itemdb-length" 
                style="padding: 3px;
                       border-radius: 15px;
                       border-color: #e4eaec;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </div>
    </h3>
    <div  class="pull-right" 
          style="margin-top: -35px;padding-right: 0px">
      <a id="itemdb-browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4" style="color:#22A7F0;"></a>
    </div>
  </div>
  <div id="accordion1_4" class="panel-collapse collapse in">
    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover table-striped" id="itemdb-retailer">
        <thead>
            <tr>
                <th>Name</th>
                <th>Model Number</th>
                {{-- <th>Product Name</th> --}}
                <th>Manufacturer</th>
                <th>Category Path</th>
              <th>Regular Price</th>
            </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
    </div>
  </div>
  <script type="text/javascript">
    $(function() {
      $('#itemdb-retailer').DataTable({
        pageLength: 10,
        oLanguage: {
          sProcessing: "<center><img src='{{ asset('vendor/tcg/voyager/assets/loader/custom_loader.gif') }}' class='img-responsive' width='70px'/></center>"
        },
        processing: false,
        serverSide: true,
        ajax: '{!! url('admin/review-upc/getItemdbRetailerAjaxDataTable?upc=') !!}'+$('#itemdb-upc').html(),
        columns: [
            { data: 'name', name: 'name' },
            { data: 'model_number', name: 'model_number' },
            /*{ data: 'name', name: 'name' },*/
            { data: 'manufacturer', name: 'manufacturer' },
            { data: 'category_path', name: 'category_path' },
            { data: 'regular_price', name: 'regular_price' },
        ]
      });

      itemdbTable = $('#itemdb-retailer').DataTable();   
      $('#itemdb-search').keyup(function(){
            itemdbTable.search($(this).val()).draw() ;
      });

      $('#itemdb-length').val(itemdbTable.page.len());
      $('#itemdb-length').change( function() { 
        itemdbTable.page.len( $(this).val() ).draw();
      });

      $('#itemdb-retailer').css('width', '100%');
    });
  </script>
</div>

{{-- ADMIN RETAILER --}}
<div class="panel panel-bordered custom-bordered row" id="admin-panel" style="padding-bottom:0px;">
  <div class="panel-heading">
    <h3 class="panel-title" style="padding-top:5px;padding-bottom:30px;">
      <i class="fa fa-tag"></i> Admin Retailer (<span id="admin-upc" style="color:#22A7F0;font-size:0.88em">{{ !empty($upc->admin_upc_override)?$upc->admin_upc_override:'' }}</span>)
      <div  class="pull-right" 
          style="margin-top: 0px;padding-right: 20px">
        
        <div class="or-search-form has-feedback" id="admin-search-panel">
          <input type="text" data-action="filter" data-filters="#original-retailer" class="form-control" id="admin-search" placeholder="Search..." style="border-radius:25px;height:27px">
          <span class="glyphicon glyphicon-search form-control-feedback" style="color:#22A7F0;line-height:29px"></span>
        </div>
      </div>
      <div  class="pull-right" 
            style="margin-top: 0px;padding-right: 8px">
        <select id="admin-length" 
                style="padding: 3px;
                       border-radius: 15px;
                       border-color: #e4eaec;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </div>
    </h3>
    <div  class="pull-right" 
          style="margin-top: -35px;padding-right: 0px">
      <a id="admin-browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_5" style="color:#22A7F0;"></a>
    </div>
  </div>
  <div id="accordion1_5" class="panel-collapse collapse in">
    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover table-striped" id="admin-retailer">
        <thead>
            <tr>
              <th>Name</th>
              <th>Model Number</th>
              {{-- <th>Product Name</th> --}}
              <th>Manufacturer</th>
              <th>Category Path</th>
              <th>Regular Price</th>
            </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
    </div>
  </div>
  <script type="text/javascript">
    $(function() {
      $('#admin-retailer').DataTable({
        pageLength: 10,
        oLanguage: {
          sProcessing: "<center><img src='{{ asset('vendor/tcg/voyager/assets/loader/custom_loader.gif') }}' class='img-responsive' width='70px'/></center>"
        },
        processing: false,
        serverSide: true,
        ajax: '{!! url('admin/review-upc/getAdminRetailerAjaxDataTable?upc=') !!}'+$('#admin-upc').html(),
        columns: [
            { data: 'name', name: 'name' },
            { data: 'model_number', name: 'model_number' },
            /*{ data: 'name', name: 'name' },*/
            { data: 'manufacturer', name: 'manufacturer' },
            { data: 'category_path', name: 'category_path' },
            { data: 'regular_price', name: 'regular_price' },
        ]
      });

      adminTable = $('#admin-retailer').DataTable();   
      $('#admin-search').keyup(function(){
            adminTable.search($(this).val()).draw() ;
      });

      $('#admin-length').val(adminTable.page.len());
      $('#admin-length').change( function() { 
        adminTable.page.len( $(this).val() ).draw();
      });

      $('#admin-retailer').css('width', '100%');

      /*hide/show html tags*/ 
      for (var i = 3; i < 6; i++) {
        if($('#accordion1_'+i).hasClass('panel-collapse collapse in')) 
        {
          $('#or-length').show();
          $('#itemdb-length').show();
          $('#admin-length').show();
          $('#or-search-panel').show();
          $('#itemdb-search-panel').show();
          $('#admin-search-panel').show();
        }
        else
        {
          $('#or-length').css('display','none');
          $('#itemdb-length').css('display','none');
          $('#admin-length').css('display','none');
          $('#or-search-panel').css('display','none');
          $('#itemdb-search-panel').css('display','none');
          $('#admin-search-panel').css('display','none');
        }
      }

      $('#or-browse').on("click", function(){
        if($('#or-browse').hasClass('accordion-toggle collapsed')) 
        {
          $('#or-length').show();
          $('#or-search-panel').show();
        }
        else
        {
          $('#or-length').css('display','none');
          $('#or-search-panel').css('display','none');
        }
      });

      $('#itemdb-browse').on("click", function(){
        if($('#itemdb-browse').hasClass('accordion-toggle collapsed')) 
        {
          $('#itemdb-length').show();
          $('#itemdb-search-panel').show();
        }
        else
        {
          $('#itemdb-length').css('display','none');
          $('#itemdb-search-panel').css('display','none');
        }
      });

      $('#admin-browse').on("click", function(){
        if($('#admin-browse').hasClass('accordion-toggle collapsed')) 
        {
          $('#admin-length').show();
          $('#admin-search-panel').show();
        }
        else
        {
          $('#admin-length').css('display','none');
          $('#admin-search-panel').css('display','none');
        }
      });



    });
  </script>
</div>

