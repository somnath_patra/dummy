<div class="panel panel-bordered custom-bordered row" style="padding-bottom:0px">
  <style type="text/css">
    .compact>tbody>tr>td, .compact>tbody>tr>th, .compact>tfoot>tr>td, .compact>tfoot>tr>th, .compact>thead>tr>td, .compact>thead>tr>th {
        padding: 0px 8px;
    }
    .compact{
      overflow: hidden;
    }
    .select2-container--default .select2-search--dropdown .select2-search__field {
      border-radius: 5px;
      border-color: #d3d8db;
    }
    .select2-container--open .select2-dropdown--below {
      border: 1px solid #d3d8db;
    }
    .select2-container .select2-selection--single {
      height: 34px;
      width: 100%;
      border: 1px solid #d3d8db;
      border-radius: 5px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
      top:4px;
    }
  </style>
  <div class="panel-heading accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1" aria-expanded="false" aria-controls="accordion1_1">
    <h3 class="panel-title" style="padding-top: 5px;padding-bottom: 30px;">
      <i class="fa fa-tag"></i> Gs1 Company Details
    </h3>
  </div>
  <div id="accordion1_1" class="panel-collapse collapse in" style="padding-bottom:5px">
    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <div style="background:linear-gradient(to bottom, #f3f7f900 0, #eee 100%);padding-bottom: 30px;">
        <div class="pull-right" style="padding-right: 10px;margin-top:5px">
          <span><b>Created At</b></span> : <small id="created_at">{{ $gs1Company->creation_date }}</small> |
          <span><b>Updated At</b></span> : <small id="updated_at">{{ $gs1Company->last_update_date }}</small>
        </div>
      </div>
      <div style="margin:10px 0px">
        <div class="col-sm-12">
          <div class="form-group col-sm-6">
            <label for="gs1_company_prefix" class="col-sm-3 text-right">
              <span class="text-danger">* </span><span class="control-label">Gs1 Prefix</span>
            </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Gs1 Prefix" id="gs1_company_prefix" value="{{ $gs1Company->gs1_company_prefix }}" readonly="">
            </div>
          </div>
          <div class="form-group col-sm-6">
            <label for="company_legal_name" class="col-sm-3 text-right">
              <span class="control-label">Company Name</span>
            </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Company Name" id="company_legal_name" value="{{ $gs1Company->company_legal_name }}">
            </div>
          </div>
        </div>

        <div class="col-sm-12">
          <div class="form-group col-sm-6">
            <label for="upc_company_prefix" class="col-sm-3 text-right">
              <span class="control-label">Upc Prefix</span>
            </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Upc Prefix" id="upc_company_prefix" value="{{ $gs1Company->upc_company_prefix }}">
            </div>
          </div>
          <div class="form-group col-sm-6">
            <label for="company_gln" class="col-sm-3 text-right">
              <span class="control-label">Gln</span>
            </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Gln" id="company_gln" value="{{ $gs1Company->company_gln }}">
            </div>
          </div>
        </div>

        <div class="col-sm-12">
          <div class="form-group col-sm-6">
            <label for="company_address" class="col-sm-3 text-right">
              <span class="control-label">Address</span>
            </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Address" id="company_address" value="{{ $gs1Company->company_address }}">
            </div>
          </div>
          <div class="form-group col-sm-6">
            <label for="country" class="col-sm-3 text-right">
              <span class="control-label">Country</span>
            </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Country" id="country" value="{{ $gs1Company->country }}">
            </div>
          </div>
        </div>

        <div class="col-sm-12">
          <div class="form-group col-sm-6">
            <label for="state_or_province" class="col-sm-3 text-right">
              <span class="control-label">State</span>
            </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="State" id="state_or_province" value="{{ $gs1Company->state_or_province }}">
            </div>
          </div>
          <div class="form-group col-sm-6">
            <label for="city" class="col-sm-3 text-right">
              <span class="control-label">City</span>
            </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="City" id="city" value="{{ $gs1Company->city }}">
            </div>
          </div>
        </div>
      </div>

      <div class="panel-footer col-sm-12">
        <button type="button" id="update" class="btn btn-primary pull-right" onclick="gs1Company.update_gs1_company(this)" data-url="{!! url('admin/gs1-company/updateGs1Company') !!}">
          {{-- <img src="{{ asset('vendor/tcg/voyager/assets/loader/ajax-loader.gif') }}" class="img-responsive" width="12px" style="display:none;" /> --}}
          <span class="update">Update</span>
          <span class="update-hidden" style="display:none;"><i class="fa fa-refresh fa-spin"></i> Updating...</span>
        </button>
      </div>
    </div>
  </div>

</div>
      
    