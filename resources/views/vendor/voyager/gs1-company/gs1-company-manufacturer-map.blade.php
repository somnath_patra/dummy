@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/search-box.css') }}">
@stop


<div class="panel panel-bordered custom-bordered row" id="or-panel" style="padding-bottom:0px;">
  <div class="panel-heading">
    <h3 class="panel-title" style="padding-top:5px;padding-bottom: 30px;">
      <i class="fa fa-sitemap"></i> Mapped Manufacturer
      <div  class="pull-right" 
            style="margin-top: -0px;padding-right: 20px">
        <button class="btn btn-primary btn-xs btn-circle" style="padding:3px 8px;margin-top: -3px" data-toggle="modal" data-target="#addModal" title="Add Mapping" data-url="{{ url('admin/gs1-company/getManufacturer?gs1_company_prefix='.$id) }}" data-id="{{ $id }}" onclick="gs1Company.get_manufacturer(this)">
          <i class="fa fa-plus"></i>
        </button>
      </div>
      <div  class="pull-right" 
          style="margin-top: -0px;padding-right: 10px">
        
        <div class="or-search-form has-feedback" id="mapping-search-panel">
          <input type="text" data-action="filter" data-filters="#mapping-table" class="form-control" id="mapping-search" placeholder="Search..." style="border-radius:25px;height:27px">
          <span class="glyphicon glyphicon-search form-control-feedback" style="color:#22A7F0;line-height:29px"></span>
        </div>
      </div>
      <div  class="pull-right" 
            style="margin-top: -0px;padding-right: 8px">
        <select id="mapping-length" 
                style="padding: 3px;
                       border-radius: 15px;
                       border-color: #e4eaec;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </div>
    </h3>
    <div  class="pull-right" 
          style="margin-top: -35px;padding-right: 0px">
      <a id="mapping-browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2" onclick="gs1Company.toggle_mappingTable_sl()" style="color:#22A7F0;"></a>
    </div>
  </div>
  <div id="accordion1_2" class="panel-collapse collapse in">
    
    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover table-striped" id="mapping-table">
        <thead>
            <tr>
                <th>Gs1 Company Prefix</th>
                <th>Manufacturer</th>
                <th>Valid</th>
                <th>Creation Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
    </div>
  </div>
</div>

{{-- COnfirm Change valid status Modal --}}
<div class="modal modal-info fade in" tabindex="-1" id="changeStatus" role="dialog" style="display: none; padding-right: 17px;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Confirm </h4>
      </div>
      <div class="modal-body">
        <div class="col-sm-12">
          <div class="col-sm-2">
            <i class="fa fa-check custom-success"></i>
          </div>
          <div class="col-sm-10">
            <p>Are you sure you want to change valid status?</p>
          </div>
          <input type="hidden" id="change-status" value="">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary pull-right" onclick="gs1Company.change_valid_status(this)" data-url="{{ url('admin/gs1-company/getValidStatus') }}">Yes, Change it!</button>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

{{-- COnfirm delete Modal --}}
<div class="modal modal-info fade in" tabindex="-1" id="deleteModal" role="dialog" style="display: none; padding-right: 17px;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Confirm </h4>
      </div>
      <div class="modal-body">
        <div class="col-sm-12">
          <div class="col-sm-2">
            <i class="fa fa-trash custom-danger"></i>
          </div>
          <div class="col-sm-10">
            <p>Are you sure you want to delete this item?</p>
          </div>
          <input type="hidden" id="delete-item" value="">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary pull-right" onclick="gs1Company.delete_mapping(this)" data-url="{{ url('admin/gs1-company/deleteMapping') }}">Yes, Delete it!</button>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

{{-- Add New Mapping --}}
<div class="modal modal-info fade in" id="addModal" role="dialog" style="display: none; padding-right: 17px;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Add New Mapping </h4>
      </div>
      <div class="modal-body">
        <div class="col-sm-12">
          <div class="col-sm-12 text-center error-div">
            <i class="fa fa-warning"></i> 
            <span class="error-msg"></span>
          </div>
          <form>
            <div class="form-group">
              <label for="gs1_company_prefix">
                <span class="text-danger">* </span><span class="control-label">Gs1 Company Prefix</span>
              </label>
              <input type="text" class="form-control" placeholder="Gs1 Company Prefix" id="gs1_company_prefix" value="{{ $id }}" readonly="">
            </div>
            <div class="form-group">
              <label for="manufacturer">
                <span class="text-danger">* </span><span class="control-label">Manufacturer</span>
              </label>
              <input id="activate_tagator2" type="text" class="tagator form-control" value="" placeholder="Manufacturer Name">
            </div>
          </form>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info pull-right" onclick="gs1Company.add_mapping(this)" data-url="{{ url('admin/gs1-company/addMapping') }}"
        data-id="{{ $id }}">
          <span class="add">Create</span>
          <span class="add-hidden" style="display:none;"><i class="fa fa-refresh fa-spin"></i> Creating...</span>
        </button>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#mapping-table').DataTable({
      pageLength: 10,
      /*oLanguage: {
        sProcessing: "<center style='margin-top:-33px'><img src='{{ asset('vendor/tcg/voyager/assets/loader/custom_loader.gif') }}' class='img-responsive' width='70px'/></center>"
      },
      processing: true,*/
      serverSide: true,
      ajax: '{!! url('admin/gs1-company/getMappingAjaxDataTable?gs1_company_prefix=') !!}'+$('#gs1_company_prefix').val(),
      columns: [
          { data: 'gs1_company_prefix', name: 'gs1_company_prefix'},
          { data: 'manufacturer', name: 'manufacturer' },
          { data: 'company_manufacturer_mapping_valid', name: 'company_manufacturer_mapping_valid',
            "render": function(data,type,row,meta) {
              if(row.company_manufacturer_mapping_valid==1){
                var a = '<button data-id="'+row.gs1_company_manufacturer_mapper_id+'" class="btn btn-circle btn-success" onclick="common.appendId(this)" style="padding:0.01em 4px;" data-toggle="modal" data-target="#changeStatus" title="Change status"><i class="fa fa-check"></i></button>'; 
              }
              else{
                var a = '<button data-id="'+row.gs1_company_manufacturer_mapper_id+'"class="btn btn-circle btn-danger" onclick="common.appendId(this)" style="padding:0.01em 5px;" data-toggle="modal" data-target="#changeStatus" title="Change status"><i class="fa fa-times"></i></button>'; 
              } 
              return a;
            },
            orderable: false
          },
          { data: 'creation_date', name: 'creation_date' },
          { data: 'action', name: 'action', orderable: false, searchable: false },
      ]
    });

    mappingTable = $('#mapping-table').DataTable();   
    $('#mapping-search').keyup(function(){
      mappingTable.search($(this).val()).draw() ;
    });

    $('#mapping-length').val(mappingTable.page.len());
    $('#mapping-length').change( function() { 
      mappingTable.page.len( $(this).val() ).draw();
    });

    $('#mapping-table').css('width', '100%');

  });

  /*$('#manufacturer').on("keypress", function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      $("#mnf_save").click();  
    }
  });*/


  $('#activate_tagator2').tagator({
    useDimmer: true,
    prefix: 'tagator_',
    height: 'auto',
    showAllOptionsOnFocus: true,  
  });

  $("#tagator_activate_tagator2").css('padding','2px 5px');


</script>
