<div class="panel panel-bordered">
	<div class="panel-heading custom-heading">
    <h5 class="page-title custom-title">
      <i class="{{ $dataType->icon }}"></i> {{-- {{ $dataType->display_name_singular }} --}}Mapping Manufacturers
    </h5>
    
    @can('add',app($dataType->model_name))
      <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new map-btn pull-right" style="margin:35px 15px 0px 0px;">
        <i class="voyager-plus" style="font-size: 14px;"></i> 
        <span>{{ __('voyager::generic.add_new') }}</span>
      </a>
    @endcan
  </div>
  <div class="panel-body">
    @if ($isServerSide)
      <form method="get" class="form-search">
        <div id="search-input">
          <select id="search_key" name="key">
            @foreach($searchable as $key)
              <option value="{{ $key }}" @if($search->key == $key){{ 'selected' }}@endif>{{ ucwords(str_replace('_', ' ', $key)) }}</option>
            @endforeach
          </select>
          <select id="filter" name="filter">
            <option value="contains" @if($search->filter == "contains"){{ 'selected' }}@endif>contains</option>
            <option value="equals" @if($search->filter == "equals"){{ 'selected' }}@endif>=</option>
          </select>
          <div class="input-group col-md-12">
            <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
            <span class="input-group-btn">
              <button class="btn btn-info btn-lg" type="submit">
                <i class="voyager-search"></i>
              </button>
            </span>
          </div>
        </div>
      </form>
    @endif
    <div class="table-responsive">
      <table id="dataTable" class="table table-hover">
        <thead>
          <tr>
           {{--  @can('delete',app($dataType->model_name))
              <th>
                <input type="checkbox" class="select_all">
              </th>
            @endcan --}}
            @foreach($dataType->browseRows as $row)
            <th>
              @if ($isServerSide)
                <a href="{{ $row->sortByUrl() }}">
              @endif
              {{ ucwords($row->display_name) }}
              @if ($isServerSide)
                @if ($row->isCurrentSortField())
                  @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                    <i class="voyager-angle-up pull-right"></i>
                  @else
                    <i class="voyager-angle-down pull-right"></i>
                  @endif
                @endif
                </a>
              @endif
            </th>
            @endforeach
            <th class="actions">{{ __('voyager::generic.actions') }}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($dataTypeContent as $data)
          <tr id="tr_{{ $data->gs1_manufacturer_mapper_id }}">
            {{-- @can('delete',app($dataType->model_name))
              <td>
                <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
              </td>
            @endcan --}}
            @foreach($dataType->browseRows as $row)
              <td>
                <?php $options = json_decode($row->details); ?>
                @if($row->type == 'image')
                  <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ str_replace('localhost', 'localhost:8000', Voyager::image( $data->{$row->field}) ) }}@else{{ $data->{$row->field} }}@endif" style="width:30px">
                @elseif($row->type == 'relationship')
                  @include('voyager::formfields.relationship', ['view' => 'browse'])
                @elseif($row->type == 'select_multiple')
                  @if(property_exists($options, 'relationship'))

                    @foreach($data->{$row->field} as $item)
                      @if($item->{$row->field . '_page_slug'})
                      <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                      @else
                      {{ $item->{$row->field} }}
                      @endif
                    @endforeach

                    {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                  @elseif(property_exists($options, 'options'))
                    @foreach($data->{$row->field} as $item)
                     {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                    @endforeach
                  @endif

                @elseif($row->type == 'date' || $row->type == 'timestamp')
                {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}

                @elseif($row->type == 'checkbox')
                  @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                    @if($data->{$row->field})
                    	<span class="label label-info">{{ $options->on }}</span>
                  	@else
                      <span class="label label-primary">{{ $options->off }}</span>
                    @endif
                  @else
                    {{ $data->{$row->field} }}
                  @endif

                @elseif($row->type == 'text')
                  @include('voyager::multilingual.input-hidden-bread-browse')
                  <div class="readmore">
                    @if(!is_array($data->{$row->field}))
                    	{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}
                    @else
                    	Locale : {{ $data->{$row->field}['locale'] }}
                    @endif
                  </div>
                
                @else
                    @include('voyager::multilingual.input-hidden-bread-browse')
                    <span>{{ $data->{$row->field} }}</span>
                @endif
              </td>
            @endforeach
            <td class="no-sort no-click" id="bread-actions">
              @foreach(Voyager::actions() as $action)
                @php $action = new $action($dataType, $data); @endphp

                @if ($action->shouldActionDisplayOnDataType())
                  @can($action->getPolicy(), $data)
                    <a href="{{ $action->getRoute($dataType->name) }}" title="{{ $action->getTitle() }}" {!! $action->convertAttributesToHtml() !!} style="padding: 1px 4px;" @if($action->getPolicy()=='delete')onclick="company.company_map_delete(this)" data-url="{{ url('admin/gis-company/companyMapDelete/'.Crypt::encrypt($data->gs1_manufacturer_mapper_id)) }}" data-token="{{ csrf_token() }}"@endif>
                      <i class="{{ $action->getIcon() }}" style="font-size: 10px;"></i>
                    </a>
                  @endcan
                @endif
              @endforeach
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    @if ($isServerSide)
        <div class="pull-left">
            <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                'voyager::generic.showing_entries', $dataTypeContent->total(), [
                    'from' => $dataTypeContent->firstItem(),
                    'to' => $dataTypeContent->lastItem(),
                    'all' => $dataTypeContent->total()
                ]) }}</div>
        </div>
        <div class="pull-right">
            {{ $dataTypeContent->appends([
                's' => $search->value,
                'filter' => $search->filter,
                'key' => $search->key,
                'order_by' => $orderBy,
                'sort_order' => $sortOrder
            ])->links() }}
        </div>
    @endif
  </div>
</div>