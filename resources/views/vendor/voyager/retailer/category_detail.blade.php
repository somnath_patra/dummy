@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/search-box.css') }}">
@stop


<div class="panel panel-bordered custom-bordered row" id="or-panel" style="padding-bottom:0px;">
  <div class="panel-heading">
    <h3 class="panel-title" style="padding-top:5px;padding-bottom: 30px;">
      <i class="fa fa-sitemap"></i> Categories
    </h3>
    <div  class="pull-right" 
          style="margin-top: -35px;padding-right: 0px">
      <a id="category-browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1" style="color:#22A7F0;"></a>
    </div>
  </div>

  <div id="accordion1_1" class="panel-collapse collapse in">
    <table class="table" style="margin: 12px 0px 0px;box-shadow: 2px 3px 5px -3px #33333387;background: #e4eaeca6;">
      <tbody>
        <tr>
          <td width="20%">
            Show 
            <select id="category-length" 
                    style="padding: 3px;
                           border-radius: 2px;
                           border-color: #e4eaec;">
              <option value="10">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="100">100</option>
            </select>
             Records
          </td>
          <td>
            <div class="or-search-form has-feedback" id="category-search-panel">
              <input type="text" data-action="filter" data-filters="#detail-table" class="form-control" id="category-search" placeholder="Search..." style="border-radius:25px;height:27px">
              <span class="glyphicon glyphicon-search form-control-feedback" style="color:#22A7F0;line-height:29px"></span>
            </div>
          </td>
          <td width="35%">
            <input id="activate_tagator2" type="text" class="tagator form-control" value="Category Name,Category Path,Parent Category,Enabled Flag,Leaf Node Flag" placeholder="Hide/Show Columns" style="padding: 2px 10px;width:100%;">
          </td>
        </tr>
      </tbody>
    </table>

    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover table-striped" id="detail-table">
        <thead>
            <tr>
                <th>Category Id</th>
                <th>Category Name</th>
                <th>Category Path</th>
                <th>Parent Category</th>
                <th>Master Category</th>
                <th>Enabled Flag</th>
                <th>Leaf Node Flag</th>
                <th>Depth</th>
                <th>Creation Date</th>
                <th>Last Update Date</th>
            </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
      <input type="hidden" id="table" value="{{ $table }}">
    </div>
  </div>
  <script type="text/javascript">
    $(function() {
      var categoryTable = $('#detail-table').DataTable({
        pageLength: 10,
        "scrollX": true,
        "sScrollXInner": "100%",
        /*oLanguage: {
          sProcessing: "<center style='margin-top:-33px'><img src='{{ asset('vendor/tcg/voyager/assets/loader/custom_loader.gif') }}' class='img-responsive' width='70px'/></center>"
        },
        processing: true,*/
        serverSide: true,
        ajax: '{!! url('admin/retailer/getCategoryAjaxDataTable/?table=') !!}'+$('#table').val(),
        columns: [
            { data: 'category_id', name: 'category_id' },
            { data: 'category_name', name: 'category_name' },
            { data: 'category_path', name: 'category_path' },
            { data: 'parent_category_id', name: 'parent_category_id' },
            { data: 'master_category_id', name: 'master_category_id' },
            { data: 'enabled_flag', name: 'enabled_flag' },
            { data: 'leaf_node_flag', name: 'leaf_node_flag' },
            { data: 'depth', name: 'depth' },
            { data: 'creation_date', name: 'creation_date' },
            { data: 'last_update_date', name: 'last_update_date' },
        ]
      });

      $('#category-search').keyup(function(){
            categoryTable.search($(this).val()).draw() ;
      });

      $('#category-length').val(categoryTable.page.len());
      $('#category-length').change( function() { 
        categoryTable.page.len( $(this).val() ).draw();
      });

      $('#detail-table').css('width', '100%');


      /*hide/show html tags*/ 
      if($('#accordion1_1').hasClass('panel-collapse collapse in')) 
      {
        $('#category-length').show();
        $('#category-search-panel').show();
      }
      else
      {
        $('#category-length').css('display','none');
        $('#category-search-panel').css('display','none');
      }

      $('#category-browse').on("click", function(){
        if($('#category-browse').hasClass('accordion-toggle collapsed')) 
        {
          $('#category-length').show();
          $('#category-search-panel').show();
        }
        else
        {
          $('#category-length').css('display','none');
          $('#category-search-panel').css('display','none');
        }
      });


      /*Radio Button Enable Flag Check*/
      $('.flag_value').on("click", function(){
        var flag = $('input[type=radio]:checked').val();
        flag = JSON.stringify(flag);
        categoryTable.search(flag).draw();
      });

      
      /*Hide/show column*/     
      var coloumn_string = {"Category_Id":0,"Category_Name":1,"Category_Path":2,"Parent_Category":3,"Master_Category":4,"Enabled_Flag":5,"Leaf_Node_Flag":6,"Depth":7,"Creation_Date":8,"Last_Update_Date":9};

      $("#activate_tagator2").on("change", function(){
        var result = $("#activate_tagator2").val().split(',');
        $.each(coloumn_string, function( index, column_value ) {
          var column = categoryTable.column( column_value );
          column.visible( false, false );
        });

        if(result!="")
        {
          $.each(result, function( index, column_name ) {
            column_name = column_name.replace(/ /g,'_');
            var column = categoryTable.column( coloumn_string[column_name] );
            column.visible( true, true );
          });
        }


      });
      /*Hide/show column*/

    });

    $('#activate_tagator2').tagator({
      useDimmer: true,
      prefix: 'tagator_',
      height: 'auto',
      showAllOptionsOnFocus: true
    });


    $(function(){
       $("#activate_tagator2").change();
    });
    
  </script>
</div>
