@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/page-css/retailer.css') }}">
  <link rel="stylesheet" href="{{ asset('public/tag-manager/tagmanager.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/fm.tagator.jquery.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/custom-dataTable.css') }}">
@stop

@section('page_header')
  <div class="container-fluid">
    <h1 class="page-title">
      <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
    </h1>
    @include('voyager::multilingual.language-selector')
    <div class="pull-right" style="margin-top:2px;">
      <button class="btn btn-info btn-md" title="Minimize All Panel" id="collapse-all"><i class="fa fa-eye"></i></button>
      <button class="btn btn-info btn-md" title="Expand All Panel" style="display: none" id="minimize-all"><i class="fa fa-eye-slash"></i></button>
    </div>
  </div>
@stop


@section('content')
  <div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
      <div class="col-md-2">
        <div class="panel panel-bordered custom-bordered" style="padding:20px 0px 0px 0px;">

          {{-- panel head --}}
          <div class="panel-heading" 
               style="padding: 0px 5px">
            <h3 class="panel-title" style="padding-top: 5px;padding-bottom: 30px;">
              <span id="list-title"><i class="fa fa-tag"></i> Retailer List</span>
              <select id="dataTable-length" 
                style="padding: 3px;
                       border-radius: 15px;
                       border-color: #e4eaec;">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
              </select>
              <button type="button" id="reset" class="btn btn-xs reset-btn" ><i class="fa fa-undo"></i></button>
            </h3>
            <div  class="pull-right" 
                  style="margin-top: -35px;padding-right: 0px">
              <a id="browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1" style="color:#22A7F0;"></a>
            </div>
          </div>
          {{-- panel head --}}
         
          <div id="accordion2_1" class="panel-collapse collapse in">
            <div class="table-responsive">

              {{-- Custom filter --}}
              <div class="col-sm-12 col-md-12 col-xs-12">
                <table class="table filter-dataTable">
                  <thead>
                    <tr>
                      <td width="100%">
                        <input type="text" 
                             data-action="filter" 
                             data-filters="#dataTable" 
                             class="form-control" 
                             id="task-table-filter" 
                             placeholder="Search..."
                             style="height:28px;width:100%;">
                        <span class="glyphicon glyphicon-search form-control-feedback" style="color: #22A7F0;top:8px;"></span>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%">
                        <div class="col-md-12" style="border-bottom: 1px solid #62a8eac2;padding-bottom: 5px;">
                          <p style="font-weight: 500;">Enabled Flag</p>
                          <input class="dropdown-item" type="radio" name="flag_filter" data-id="enabled_flag" value="true" checked> True
                          <input class="dropdown-item" type="radio" name="flag_filter" data-id="enabled_flag" value="false"> False
                          <input class="dropdown-item" type="radio" name="flag_filter" data-id="enabled_flag" value=""> Any
                        </div>
                        <div class="col-md-12" style="border-bottom: 1px solid #62a8eac2;padding: 5px;">
                          <p style="font-weight: 500;">Api Enabled</p>
                          <input class="dropdown-item" type="radio" name="api_filter" data-id="api_enabled" value="true"> True
                          <input class="dropdown-item" type="radio" name="api_filter" data-id="api_enabled" value="false"> False
                          <input class="dropdown-item" type="radio" name="api_filter" data-id="api_enabled" value="" checked> Any
                        </div>
                        <div class="col-md-12" style="padding-top: 5px;">
                          <p style="font-weight: 500;">Crawl Enabled</p>
                          <input class="dropdown-item" type="radio" name="crawl_filter" data-id="crawl_enabled" value="true"> True
                          <input class="dropdown-item" type="radio" name="crawl_filter" data-id="crawl_enabled" value="false"> False
                          <input class="dropdown-item" type="radio" name="crawl_filter" data-id="crawl_enabled" value="" checked> Any
                        </div>
                      </td>
                    </tr>
                  </thead>
                </table>
              </div>
              {{-- Custom filter --}}

              {{-- main content --}}
              <table class="table table-hover" id="dataTable">
                <thead>
                  
                </thead>
                <tbody>
                  
                </tbody>
              </table>
              {{-- main content --}}

            </div>
          </div>
        </div>
      </div>

      <div class="col-md-10">
        <center id="custom-loader" style="display: none;">
          <img src="{{ asset('public/vendor/tcg/voyager/assets/loader/custom_loader.gif') }}" class="img-responsive" width="70px"/>
        </center>
        <div class="panel-group" id="accordion1" aria-multiselectable="true">
          <!-- Retailer Xpath Details Start -->
          <div class="xpath col-sm-12"></div>
          <!-- Retailer Xpath Details Ends -->

          {{-- Cron Schedule --}}
          <div class="col-sm-12 cron-count"></div>
          {{-- Cron Schedule --}}
          
          <div class="col-sm-12">
            <!-- Category Details Start -->
            <div class="col-sm-3 row retailer-category animate fadeInDown one" style="display:none;padding-left: 0px"></div>
            <!-- Category Details Ends -->

            <!-- Products Details Start -->
            <div class="col-sm-3 row retailer-product animate fadeInUp two" style="display:none;padding-right: 0px;margin-left:20px"></div>
            <!-- Products Details Ends -->

            <!-- Category Details Start -->
            <div class="col-sm-3 row retailer-offer animate fadeInLeft three" style="display:none;padding-left: 0px;margin-left:30px"></div>
            <!-- Category Details Ends -->

            <!-- Category Details Start -->
            <div class="col-sm-3 row retailer-promotion animate fadeInRight four" style="display:none;padding-right: 0px;margin-left:20px"></div>
            <!-- Category Details Ends -->
          </div>
          <!-- Category Details Start -->
          <div class="details col-sm-12"></div>
          <!-- Category Details Ends -->
        </div>
      </div>
    </div>
  </div>
@stop

@section('javascript')
  {{-- Page Level JS --}}
  <script type="text/javascript" src="{{ asset('public/js/custom/retailer/retailer.js') }}"></script>
  <script type="text/javascript" src="{{ asset('public/tag-manager/tagmanager.js') }}"></script>
  <script src="{{ asset('public/select2/select2.min.js') }}"></script>
  <script src="{{ asset('public/js/fm.tagator.jquery.js') }}"></script>

  <!-- DataTables -->
  <script src="{{ asset('public/js/custom/retailer/retailer-dataTable.js') }}"></script>
@stop
