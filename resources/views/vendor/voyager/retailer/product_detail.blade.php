@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/search-box.css') }}">
@stop


<div class="panel panel-bordered custom-bordered row" id="or-panel" style="padding-bottom:0px;">
  <div class="panel-heading">
    <h3 class="panel-title" style="padding-top:5px;padding-bottom: 30px;">
      <i class="fa fa-gift"></i> Products
    </h3>
    <div  class="pull-right" 
          style="margin-top: -35px;padding-right: 0px">
      <a id="product-browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1" style="color:#22A7F0;"></a>
    </div>
  </div>
  <div id="accordion1_1" class="panel-collapse collapse in">
    

    {{-- filters --}}
    <table class="table" style="margin: 12px 0px 0px;box-shadow: 2px 3px 5px -3px #33333387;background: #e4eaeca6;">
      <tbody>
        <tr>
          <td width="20%">
            Show 
            <select id="product-length" 
                    style="padding: 3px;
                           border-radius: 15px;
                           border-color: #e4eaec;">
              <option value="10">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="100">100</option>
            </select>
             Records
          </td>
          <td>
            <div class="or-search-form has-feedback" id="product-search-panel">
              <input type="text" data-action="filter" data-filters="#detail-table" class="form-control" id="product-search" placeholder="Search..." style="border-radius:25px;height:27px">
              <span class="glyphicon glyphicon-search form-control-feedback" style="color:#22A7F0;line-height:29px"></span>
            </div>
          </td>
          <td width="35%">
            <input id="activate_tagator2" type="text" class="tagator form-control" value="Product Name,Model Number,Retailer Upc,Date Released,Regular Price,Sale Price,Shipping Cost" placeholder="Hide/Show Columns" style="padding: 2px 10px;width:100%;">
          </td>
        </tr>
      </tbody>
    </table>
    {{-- filters --}}

    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover table-striped" id="detail-table">
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Model Number</th>
                <th>Retailer Upc</th>
                <th>Date Released</th>
                <th>Regular Price</th>
                <th>Sale Price</th>
                <th>Shipping Cost</th>
            </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
      <input type="hidden" id="table" value="{{ $table }}">
    </div>
  </div>
  <script type="text/javascript">
    $(function() {
      $('#detail-table').DataTable({
        pageLength: 10,
        "scrollX": true,
        "sScrollXInner": "100%",
        /*oLanguage: {
          sProcessing: "<center style='margin-top:-33px'><img src='{{ asset('vendor/tcg/voyager/assets/loader/custom_loader.gif') }}' class='img-responsive' width='70px'/></center>"
        },
        processing: true,*/
        serverSide: true,
        ajax: '{!! url('admin/retailer/getProductAjaxDataTable/?table=') !!}'+$('#table').val(),
        columns: [
            { data: 'name', name: 'name' },
            { data: 'model_number', name: 'model_number' },
            { data: 'retailer_upc', name: 'retailer_upc' },
            { data: 'date_released', name: 'date_released' },
            { data: 'regular_price', name: 'regular_price' },
            { data: 'sale_price', name: 'sale_price' },
            { data: 'shipping_cost', name: 'shipping_cost' },
        ]
      });

      productTable = $('#detail-table').DataTable();   
      $('#product-search').keyup(function(){
            productTable.search($(this).val()).draw() ;
      });

      $('#product-length').val(productTable.page.len());
      $('#product-length').change( function() { 
        productTable.page.len( $(this).val() ).draw();
      });

      $('#detail-table').css('width', '100%');

      /*hide/show html tags*/ 
      if($('#accordion1_1').hasClass('panel-collapse collapse in')) 
      {
        $('#product-length').show();
        $('#product-search-panel').show();
      }
      else
      {
        $('#product-length').css('display','none');
        $('#product-search-panel').css('display','none');
      }

      $('#product-browse').on("click", function(){
        if($('#product-browse').hasClass('accordion-toggle collapsed')) 
        {
          $('#product-length').show();
          $('#product-search-panel').show();
        }
        else
        {
          $('#product-length').css('display','none');
          $('#product-search-panel').css('display','none');
        }
      });

      /*Hide/show column*/     
      var coloumn_string = {"Product_Name":0,"Model_Number":1,"Retailer_Upc":2,"Date_Released":3,'Regular_Price':4,"Sale_Price":5,"Shipping_Cost":6}

      $("#activate_tagator2").on("change", function(){
        var result = $("#activate_tagator2").val().split(',');
        $.each(coloumn_string, function( index, column_value ) {
          var column = productTable.column( column_value );
          column.visible( false, false );
        });

        if(result!="")
        {
          $.each(result, function( index, column_name ) {
            column_name = column_name.replace(/ /g,'_');
            var column = productTable.column( coloumn_string[column_name] );
            column.visible( true, true );
          });
        }


      });
      /*Hide/show column*/

    });

    $('#activate_tagator2').tagator({
      useDimmer: true,
      prefix: 'tagator_',
      height: 'auto',
      showAllOptionsOnFocus: true
    });
  </script>
</div>
