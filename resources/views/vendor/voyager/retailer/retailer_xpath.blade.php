@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/search-box.css') }}">
@stop


<div class="panel panel-bordered custom-bordered row" id="or-panel" style="padding-bottom:0px;">
  <div class="panel-heading">
    <h3 class="panel-title" style="padding-top:5px;padding-bottom: 30px;">
      <i class="fa fa-sitemap"></i> Retailer-xpath
      <div  class="pull-right" 
            style="margin-top: -0px;padding-right:22px">
        <select id="xpath-length" 
                style="padding: 3px;
                       border-radius: 15px;
                       border-color: #e4eaec;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </div>
      <div  class="pull-right" 
            style="margin-top: -1px;padding-right:10px">
        <style type="text/css">
          .dropdown-menu > li > .dropdown-menu-item.checkbox input {
              display: none;
          }
        </style>
        {{-- <div class="btn-group">
          <button type="button" class="btn table-filter dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-filter" style="color:#22A7F0"></span> 
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" style="left:-55px;margin:2px;padding:0px;">
            <li style="font-size:12px;margin:0px;background-color:#22A7F0;color:#fff;">Show / Hide Columns</li>
            <li role="separator" class="divider" style="margin: 0px;"></li> 
            <li>
              <label class="dropdown-menu-item checkbox">
                <input type="checkbox" data-position="0"/>
                <span class="glyphicon glyphicon-unchecked filter-checkbox"></span>
                Category Name
              </label>
            </li>
            <li>
              <label class="dropdown-menu-item checkbox">
                <input type="checkbox" data-position="1"/>
                <span class="glyphicon glyphicon-unchecked filter-checkbox"></span>
                Category Path
              </label>
            </li>
            <li>
              <label class="dropdown-menu-item checkbox">
                <input type="checkbox" data-position="2"/>
                <span class="glyphicon glyphicon-unchecked filter-checkbox"></span>
                Parent Category
              </label>
            </li> 
            <li>
              <label class="dropdown-menu-item checkbox">
                <input type="checkbox" data-position="3"/>
                <span class="glyphicon glyphicon-unchecked filter-checkbox"></span>
                Enabled Flag
              </label>
            </li>
            <li>
              <label class="dropdown-menu-item checkbox">
                <input type="checkbox" data-position="4"/>
                <span class="glyphicon glyphicon-unchecked filter-checkbox"></span>
                Leaf Node Flag
              </label>
            </li>
            <li role="separator" class="divider" style="margin: 0px;"></li>               
            <li style="font-size:12px;margin:0px;background-color:#22A7F0;color:#fff;">Filter</li>
            <li>
              <label class="dropdown-menu-item" style="padding:0px">
                Enabled Flag
                <input type="radio" value="1" name="flag_value" class="flag_value" style="margin-left: 10px" /><strong style="font-weight: 600;"> 1</strong>
                <input type="radio" value="0" name="flag_value" class="flag_value" /><strong style="font-weight: 600;"> 0</strong>
              </label>
            </li>
          </ul>
        </div> --}}
      </div>
      <div  class="pull-right" 
          style="margin-top: -0px;padding-right: 10px">
        
        <div class="or-search-form has-feedback" id="xpath-search-panel">
          <input type="text" data-action="filter" data-filters="#xpath-table" class="form-control" id="xpath-search" placeholder="Search..." style="border-radius:25px;height:27px">
          <span class="glyphicon glyphicon-search form-control-feedback" style="color:#22A7F0;line-height:29px"></span>
        </div>
      </div>
    </h3>
    <div  class="pull-right" 
          style="margin-top: -35px;padding-right: 0px">
      <a id="xpath-browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1" style="color:#22A7F0;"></a>
    </div>
  </div>
  <div id="accordion1_1" class="panel-collapse collapse in">
    
    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover table-striped" id="xpath-table">
        <thead>
            <tr>
                <th>Retailer Name</th>
                <th>Field</th>
                <th>Xpath</th>
                <th>Page Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
          
        </tbody>
        <input type="hidden" id="retailer_id" value="{{ $id }}">
      </table>
    </div>
  </div>


  {{-- Edit Modal --}}
  <div class="modal modal-info fade in" tabindex="-1" id="editModal" role="dialog" style="display: none; padding-right: 17px;">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Update Retailer Xpath </h4>
        </div>
        <div class="modal-body">
          <div class="col-sm-12">
            <form>
              <div class="form-group">
                <label class="control-label col-sm-12" for="field">Field:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="field" placeholder="Enter field name">
                </div>
              </div>
              <div class="clearfix" style="margin:10px 0px;"></div>
              <div class="form-group">
                <label class="control-label col-sm-12" for="xpath">Xpath:</label>
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="xpath" placeholder="Enter xpath name">
                </div>
              </div>
            </form>
            <input type="hidden" id="edit-item" value="">
          </div>
        </div>
        <div class="clearfix" style="margin: 20px 0px;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-right" onclick="retailer.update_retailer_xpath(this)" data-url="{{ url('admin/retailer/updateRetailerXpath') }}">
            <span class="update">Update</span>
            <span class="update-hidden" style="display:none;"><i class="fa fa-refresh fa-spin"></i> Updating...</span>
          </button>
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>



  <script type="text/javascript">
    $(function() {
      $('#xpath-table').DataTable({
        pageLength: 10,
        /*oLanguage: {
          sProcessing: "<center style='margin-top:-33px'><img src='{{ asset('vendor/tcg/voyager/assets/loader/custom_loader.gif') }}' class='img-responsive' width='70px'/></center>"
        },
        processing: true,*/
        serverSide: true,
        ajax: '{!! url('admin/retailer/getXpathAjaxDataTable?id=') !!}'+$('#retailer_id').val(),
        columns: [
            { data: 'retailer.retailer_name', name: 'retailer_name' },
            { data: 'field', name: 'field' },
            { data: 'xpath', name: 'xpath' },
            { data: 'page_type', name: 'page_type' },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ]
      });

      xpathTable = $('#xpath-table').DataTable();   
      $('#xpath-search').keyup(function(){
            xpathTable.search($(this).val()).draw() ;
      });

      $('#xpath-length').val(xpathTable.page.len());
      $('#xpath-length').change( function() { 
        xpathTable.page.len( $(this).val() ).draw();
      });

      $('#xpath-table').css('width', '100%');

      /*hide/show html tags*/ 
      if($('#accordion1_1').hasClass('panel-collapse collapse in')) 
      {
        $('#xpath-length').show();
        $('#xpath-search-panel').show();
      }
      else
      {
        $('#xpath-length').css('display','none');
        $('#xpath-search-panel').css('display','none');
      }

      $('#xpath-browse').on("click", function(){
        if($('#xpath-browse').hasClass('accordion-toggle collapsed')) 
        {
          $('#xpath-length').show();
          $('#xpath-search-panel').show();
        }
        else
        {
          $('#xpath-length').css('display','none');
          $('#xpath-search-panel').css('display','none');
        }
      });


      $( '.dropdown-menu li' ).on( 'click', function( event ) {
        var checkbox = $(this).find('.checkbox');
        if (!checkbox.length) {
            return;
        }
        var input = checkbox.find('input');
        var icon = checkbox.find('span.glyphicon');

        if (input.is(':checked')) {
            input.prop('checked',false);
            icon.removeClass('glyphicon-check').addClass('glyphicon-unchecked');
            var position = $(input).attr('data-position');
            // Get the column API object
            var column = xpathTable.column( position );
            column.visible( ! column.visible() );
            // Toggle the visibility
        } else {
            input.prop('checked',true);
            icon.removeClass('glyphicon-unchecked').addClass('glyphicon-check')
            var position = $(input).attr('data-position');
            
            // Get the column API object
            var column = xpathTable.column( position );
            column.visible( ! column.visible() );
            // Toggle the visibility
        }
        return false;
      }); 

      /*Radio Button Enable Flag Check*/
      $('.flag_value').on("click", function(){
        var flag = $('input[type=radio]:checked').val();
        flag = JSON.stringify(flag);
        xpathTable.search(flag).draw();
      });


    });
  </script>
</div>
