<!-- small box -->
<div class="small-box bg-aqua">
  <div class="inner">
    <h3>{{ $promotion_count }}</h3>

    <p>PROMOTION</p>
  </div>
  <div class="icon">
    <i class="fa fa-qrcode"></i>
  </div>
  <a data-url="{{ url('admin/retailer/getPromotionDetails') }}" id="get-promotion-details" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
  </a>
</div>