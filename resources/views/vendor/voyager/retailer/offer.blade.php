<!-- small box -->
<div class="small-box bg-aqua">
  <div class="inner">
    <h3>{{ $offer_count }}</h3>

    <p>OFFERS</p>
  </div>
  <div class="icon">
    <i class="fa fa-align-left"></i>
  </div>
  <a data-url="{{ url('admin/retailer/getOfferDetails') }}" id="get-offer-details" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
  </a>
</div>