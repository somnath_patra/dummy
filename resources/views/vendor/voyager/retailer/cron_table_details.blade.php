@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/search-box.css') }}">
@stop


<div class="panel panel-bordered custom-bordered row" id="or-panel" style="padding-bottom:0px;">
  <div class="panel-heading">
    <h3 class="panel-title" style="padding-top:5px;padding-bottom: 30px;">
      <i class="fa fa-sitemap"></i> Categories
    </h3>
    <div  class="pull-right" 
          style="margin-top: -35px;padding-right: 0px">
      <a id="category-browse" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1" style="color:#22A7F0;"></a>
    </div>
  </div>

  <div id="accordion1_1" class="panel-collapse collapse in">
    <table class="table" style="margin: 12px 0px 0px;box-shadow: 2px 3px 5px -3px #33333387;background: #e4eaeca6;">
      <tbody>
        <tr>
          <td width="20%">
            Show 
            <select id="category-length" 
                    style="padding: 3px;
                           border-radius: 2px;
                           border-color: #e4eaec;">
              <option value="10">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="100">100</option>
            </select>
             Records
          </td>
          <td>
            <div class="or-search-form has-feedback" id="category-search-panel">
              <input type="text" data-action="filter" data-filters="#detail-table" class="form-control" id="category-search" placeholder="Search..." style="border-radius:25px;height:27px">
              <span class="glyphicon glyphicon-search form-control-feedback" style="color:#22A7F0;line-height:29px"></span>
            </div>
          </td>
          <td width="35%">
            <input id="activate_tagator2" type="text" class="tagator form-control" value="Category Name,Category Path,Parent Category,Enabled Flag,Leaf Node Flag" placeholder="Hide/Show Columns" style="padding: 2px 10px;width:100%;">
          </td>
        </tr>
      </tbody>
    </table>

    <div class="table-responsive" style="border:1px solid #d3d8db;padding:0px;margin:10px 0px 10px 0px;border-radius:5px;">
      <table class="table table-hover table-striped" id="detail-table">
        <thead>
            <tr>
                <th>Category Id</th>
                <th>Category Name</th>
                <th>Category Path</th>
                <th>Parent Category</th>
                <th>Master Category</th>
                <th>Enabled Flag</th>
                <th>Leaf Node Flag</th>
                <th>Depth</th>
                <th>Creation Date</th>
                <th>Last Update Date</th>
            </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
      <input type="hidden" id="table" value="{{ $table }}">
    </div>
  </div>
  <script type="text/javascript">
 
  </script>
</div>
