@php 
  $count = count($cron_data['table']);
@endphp
@php
  $total_count = array();
  for($j=0;$j<$count ; $j++)
  { 
    if($cron_data['table'][$j]=='0')
    {
      $total_count[] = $cron_data['table'][$j];
    }    
  }  
@endphp
@if(count($total_count)!=$count)
@for($i=0;$i<$count ; $i++)
  <div class="col-sm-3">
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3>{{ $cron_data['count'][$i] }}</h3>

        <p>{{ $cron_data['table'][$i] }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-sitemap"></i>
      </div>
      <a onclick="CronTableDetails('{{$cron_data["table"][$i]}}')" id="get-category-details" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
@endfor
@else
<div class="col-sm-12 alert alert-danger">No Record Found  </div>
@endif