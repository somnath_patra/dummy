

<!-- small box -->
<div class="small-box bg-aqua">
  <div class="inner">
    <h3>{{ $category_count }}</h3>

    <p>CATEGORIES</p>
  </div>
  <div class="icon">
    <i class="fa fa-sitemap"></i>
  </div>
  <a data-url="{{ url('admin/retailer/getCategoryDetails') }}" id="get-category-details" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
  </a>
</div>
