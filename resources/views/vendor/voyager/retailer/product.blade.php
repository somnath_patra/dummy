

<!-- small box -->
<div class="small-box bg-aqua">
  <div class="inner">
    <h3>{{ $product_count }}</h3>

    <p>PRODUCTS</p>
  </div>
  <div class="icon">
    <i class="fa fa-gift"></i>
  </div>
  <a data-url="{{ url('admin/retailer/getProductDetails') }}" id="get-product-details" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
  </a>
</div>

