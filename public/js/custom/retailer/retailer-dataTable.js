$(document).ready(function () {
  /*Server side Datatable*/
  $(function() {
    $('#dataTable').DataTable({
      processing: false,
      serverSide: true,
      ajax: BASE_URL + 'admin/retailer/getRetailerAjaxDataTable',
      columns: [
          { data: 'retailer_name', name: 'retailer_name',
          "render": function(data,type,row,meta) {  
              var a = '<a class="get-details" data-table="'+row.retailer_cron_schedule +'" data-id="'+row.retailer_id+'" title="View Details">' + row.retailer_name +'</a>'; 
              return a;
            }/*,
            orderable:false*/
          }
      ],
      "pagingType": "simple",
      "initComplete": function(settings, json) {
        $("#dataTable thead tr th").css("width","100%");
        $("#dataTable tbody").find("tr:first a").trigger('click');
        //$('#dataTable_next').before('<li class="paginate_button" style="display:inline;"><a href="#" aria-controls="dataTable"></a></li>');
      }
    });

    oTable = $('#dataTable').DataTable();   
    $('#task-table-filter').keyup(function(){
      oTable.search($(this).val()).draw();
    });

    $('#dataTable-length').val(oTable.page.len());
    $('#dataTable-length').change( function() { 
      oTable.page.len( $(this).val() ).draw();
    });

    oTable.on( 'draw', function () {
      
      $(".get-details").on("click", function(){
        $("tr").removeClass("active");
        $(this).parent().closest("tr").addClass("active");
        retailer.xpath_details(this);
      });
    });

    $('#dataTable').css('width','100%');
    $("#dataTable_info").parent().closest('div').css('display', 'none');
    $("#dataTable_paginate").parent().closest('div').attr('class', 'col-sm-12');
    $("#dataTable_paginate").css('float','left');
    
    retailer.search_enable_flag();
  });
  

  /*Custom Filter*/
  $(".dropdown-item").on("change", function(){

    var searchby = new Array();
    var value = '';
    if($("input[name='api_filter']").is(':checked'))
    {
      value = $("input[name='api_filter']:checked").val();
      searchby.push({value:value, index:$("input[name='api_filter']").attr("data-id")});
    }
    if($("input[name='flag_filter']").is(':checked'))
    {
      value = $("input[name='flag_filter']:checked").val();
      searchby.push({value:value, index:$("input[name='flag_filter']").attr("data-id")});
    }
    if($("input[name='crawl_filter']").is(':checked'))
    {
      value = $("input[name='crawl_filter']:checked").val();
      searchby.push({value:value, index:$("input[name='crawl_filter']").attr("data-id")});
    }
    var d = JSON.stringify(searchby);
    oTable.search(d).draw();
      
  });




  /*Reset filter*/
  $('#reset').on("click", function(){
    $("input[type='radio']").prop('checked',false);
    $("input[name='flag_filter'][value='true']").prop('checked', true);
    $("input[name='api_filter'][value='']").prop('checked', true);
    $("input[name='crawl_filter'][value='']").prop('checked', true);
    $('#task-table-filter').val('');
    retailer.search_enable_flag();
  });



  /*hide/show html tags*/ 
  if($('#accordion2_1').hasClass('panel-collapse collapse in')) 
  {
    $('#list-title').css('display','none');
  }

  $('#browse').on("click", function(){
    if($('#browse').hasClass('accordion-toggle collapsed')) 
    {
      $('#dataTable-length').show();
      $('#list-title').hide();
    }
    else
    {
      $('#dataTable-length').hide();
      $('#list-title').show();
    }
  });

  $('#collapse-all').on("click", function(){
    $('#collapse-all').css('display', 'none')
    $('#minimize-all').show();
    $('.accordion-toggle').addClass('collapsed');
    $('.panel-collapse').removeClass('in');

    $('#category-length').css('display','none');
    $('#category-search-panel').css('display','none');
    $('#product-length').css('display','none');
    $('#product-search-panel').css('display','none');
    $('#offer-length').css('display','none');
    $('#offer-search-panel').css('display','none');
    $('#promotion-length').css('display','none');
    $('#promotion-search-panel').css('display','none');
  });

  $('#minimize-all').on("click", function(){
    $('#minimize-all').css('display', 'none')
    $('#collapse-all').show();
    $('.accordion-toggle').removeClass('collapsed');
    $('.panel-collapse').addClass('in');

    $('#category-length').show();
    $('#category-search-panel').show();
    $('#product-length').show();
    $('#product-search-panel').show();
    $('#offer-length').show();
    $('#offer-search-panel').show();
    $('#promotion-length').show();
    $('#promotion-search-panel').show();
  });

});


