
var retailer = {
	/*get category count*/
  'xpath_details' : function(pointer){
		/*DEFAULT content to be empty and display none*/
	  	$('.retailer-category').html('').css('display','none');
	  	$('.retailer-product').html('').css('display','none');
	  	$('.retailer-offer').html('').css('display','none');
	  	$('.retailer-promotion').html('').css('display','none');
	  	$('.xpath').html('').css('display','none');
	  	$('.cron-count').html('').css('display','none');

	  	/*If data available for detail view otherwise empty*/
	  	$('.details').html('');

	  	var id = pointer.getAttribute('data-id');
	  	var data_table = pointer.getAttribute('data-table');

	  	$('#custom-loader').show();						/*show loader*/

	  	var crawl = $("input[name='crawl_filter']:checked").val();

	  	if(crawl=='true')
	  	{
		  	$.ajax({
			  url: BASE_URL + 'admin/retailer/getXpathDetails',
			  type:'get',
			  data: {id : id},

			  success:function(response){
			  	
				$('.xpath').html(response);
				/*call get_product*/ 
				retailer.get_cronTable_count(data_table);
			  }
			});
	  	}
	  	else
	  	{
	  		retailer.get_cronTable_count(data_table);
	  	}

	},
	/*get cron schedule count*/
  	'get_cronTable_count' : function(data_table){
		
		$.ajax({
		  url: BASE_URL + 'admin/retailer/getCronTableCount',
		  type:'POST',
		  data : {data_table : data_table},

		  success:function(response){

		    /*Append Category Count*/
		    $('.cron-count').html(response);

		  },
		  complete : function(){
		  	$('#custom-loader').hide();
		  	$('.xpath').css('display','block');
	  		$('.cron-count').css('display','block');
		  }
		});
	},
	
	'search_enable_flag' : function(){
        var searchby1 = new Array();
        var value1 = $("input[name='flag_filter']:checked").val();
        searchby1.push({value:value1, index:$("input[name='flag_filter']").attr("data-id")});
        var d1 = JSON.stringify(searchby1);
        oTable.search(d1).draw();
    }
};
function CronTableDetails(table){
  alert(table);
  $.ajax({
		  url: BASE_URL + 'admin/retailer/CronTableDetails',
		  type:'POST',
		  data : {data_table : table},

		  success:function(response){
		  	alert(response);return false;
		   
		  //  $('.cron-count').html(response);

		  }
		  
		});
}