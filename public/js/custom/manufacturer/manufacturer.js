$( document ).ready(function() {

  $('.get-details').click(function(){
  	var url = this.getAttribute('data-url');
  	var id = this.getAttribute('data-id');
  	manufacturer.get_details(url,id);
  });

  /*Ajax loader*/
  $(document).ajaxStart(function(){
  	$("#voyager-loader").css("display", "block");
  });

  $(document).ajaxComplete(function(){
  	$("#voyager-loader").css("display", "none");
  });
  /*Ajax loader*/

});


var manufacturer = {
  'get_details' : function(url,id){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){

		    /*Change url for Mapping view*/
			url = url.replace("getDetails", "getMappings");
			/*call get_mappings*/ 
			manufacturer.get_mappings(url,id);

		    /*Append Company Details*/
		    $('.manufacturer-details').html(response);
		  }
		});
	},
	'get_mappings' : function(url,id){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){

		  	/*Change url for Retailer Mapping view*/
			url = url.replace("getMappings", "getRetailerMappings");
			/*call get_retailer_mappings*/ 
			manufacturer.get_retailer_mappings(url,id);

		    /*Append Company Mapping*/
		  	$('.company-manufacturer-mapping').html(response);
		  }
		});
	},
	'get_retailer_mappings' : function(url,id){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){

		    /*Append retailer Mapping*/
		  	$('.retailer-manufacturer-mapping').html(response);
		  }
		});
	},
	'manufacturer_map_delete' : function(pointer){
		var id = pointer.getAttribute('data-id');
		alert(url+'/'+id)
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){

		    /*Append retailer Mapping*/
		  	$('.retailer-manufacturer-mapping').html(response);
		  }
		});
	}
};