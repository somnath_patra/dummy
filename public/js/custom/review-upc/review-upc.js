$( document ).ready(function() {

  /*$('.get-details').click(function(){
  	var url = this.getAttribute('data-url');
  	var id = this.getAttribute('data-id');
  	reviewUpc.get_upc_title(url,id);
  });*/

  /*Ajax loader*/
  /*$(document).ajaxStart(function(){
  	$("#voyager-loader").css("display", "block");
  });

  $(document).ajaxComplete(function(){
  	$("#voyager-loader").css("display", "none");
  });*/
  /*Ajax loader*/

  	
});

var reviewUpc = {
  'get_upc_title' : function(pointer){
  	$('.get-upc-title').hide();
  	$('.get-tags').hide();
  	$('.get-upc').hide();
  	$('.get-retailer').hide();
  	var url = pointer.getAttribute('data-url');
  	var id = pointer.getAttribute('data-id');
  	
  	$('#custom-loader').show();						/*show loader*/
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		  	$('.custom-active').css({'background-color':'#fff','color':'#22A7F0'});
		  	$('#custom-active_'+id).css({'background-color':'#19B5FE','color':'#fff'});
		   
		    /*Append Upc Title Details*/
		    $('.get-upc-title').html(response);
		    $('.breadcrumb').html('<li class="active"><a href="http://127.0.0.1:8000/admin"><i class="voyager-boat"></i> Dashboard</a></li><li><a href="http://127.0.0.1:8000/admin/review-upc"> Review Upc</a></li><li>'+$('.upc-title').html()+'</li>');

			/*GET upc title*/
			var upc_title = $('.upc-title').html();

		    /*Change url for tags view*/
			url = url.replace("getUpcTitle", "getTags");
			/*call get_tags*/ 
			reviewUpc.get_tags(url,upc_title);

		  }
		});
	},
	'get_tags' : function(url,upc_title){


		$.ajax({
		  url: url,
		  type:'POST',
		  data: {upc_title : upc_title},
		  success:function(response){

		  	/*Change url for Mapping view*/
				url = url.replace("getTags", "getUpc");
				/*call get_tags*/ 
				reviewUpc.get_upc(url,upc_title);

		  	$('.get-tags').html(response);
		  }
		});
	},
	'get_upc' : function(url,upc_title){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {upc_title : upc_title},
		  success:function(response){
		  	
		  	/*Change url for Mapping view*/
				url = url.replace("getUpc", "getRetailer");
				/*call get_tags*/ 
				reviewUpc.get_retailer(url,upc_title);

		  	$('.get-upc').html(response);
		  }
		});
	},
	'get_retailer' : function(url,upc_title){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {upc_title : upc_title},
		  success:function(response){
		  	
		  	$('.get-retailer').html(response);
		  },
		  complete: function(){
	        $('#custom-loader').hide();			/*loading complete hide loader*/
	      	$('.get-upc-title').show();
		  	$('.get-tags').show();
		  	$('.get-upc').show();
		  	$('.get-retailer').show();
	      }
		});
	},
	'company_map_delete' : function(pointer){
		var id = pointer.getAttribute('data-id');
		var url = pointer.getAttribute('data-url');
		$('#delete_modal').modal('show');
		
		$('#confirm_delete').click(function(){
			$('#delete_modal').modal('hide');
			
			$.ajax({
			  url: url,
			  type:'POST',
			  data: {id : id},
			  success:function(response){
			  	toastr.success("Successfully Deleted Gis Company Manufacturer Mapper");
			  	$('#tr_'+id).fadeOut('slow');
			  }
			});
		});
	},
	'selected_tags': function(id){
		if($("#"+id+"-override").val() != '')
		{
			$("#"+id+"-append span").each(function(index, elem){
			  if($("#"+id+"-override").val() == $(this).find("span").text())
			  {
			    $("#"+id+"-append span a").html('x');
			    $(this).find("a").html('');
			  }
			});

			$("#pre-"+id+"-tag span").each(function(index, elem){
			  if($("#"+id+"-override").val() == $(this).text())
			  {
			    $("#"+id+"-append span a").html('x');
			  }
			});
		}
	}
};