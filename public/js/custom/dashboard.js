var dashboard = {
	'get_last_updated_table' : function(id){
		$.ajax({
		  url: BASE_URL + 'getLastUpdatedTime',
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		  	$('.table-updated-time').html(response)
		    
		  }
		});
	}	
};


$(function(){
	$(".retailer_list").on("click", function(){
		var id = this.getAttribute('data-id');
		dashboard.get_last_updated_table(id);
	});

	$("tr:first").removeClass("active");
  	$("tr:first").addClass("active");
  	var id = $("tr:first td").attr('data-id');
	dashboard.get_last_updated_table(id);

});