$(document).ready(function () {
  /*Server side Datatable*/
  $(function() {
    $('#dataTable').DataTable({
      processing: false,
      serverSide: true,
      ajax: BASE_URL + 'admin/upc-review/getUpcListAjaxDataTable',
      columns: [
          { data: 'retailer_upc', name: 'retailer_upc',
          "render": function(data,type,row,meta) {  
              var a = '<a class="get-details" data-name="'+row.retailer_upc+'" data-id="'+row.retailer_upc+'" title="View Details">' + row.retailer_upc +'</a>'; 
              return a;
            }/*,
            orderable:false*/
          }
      ],
      "initComplete": function(settings, json) {
        $("#dataTable thead tr th").css("width","100%");
        $("#dataTable tbody").find("tr:first a").trigger('click');
      }
    });

    oTable = $('#dataTable').DataTable();   
    $('#task-table-filter').keyup(function(){
      oTable.search($(this).val()).draw();
    });

    $('#dataTable-length').val(oTable.page.len());
    $('#dataTable-length').change( function() { 
      oTable.page.len( $(this).val() ).draw();
    });

    oTable.on( 'draw', function () {
      
      $(".get-details").on("click", function(){
        $("tr").removeClass("active");
        $(this).parent().closest("tr").addClass("active");
        upcReview.get_upc_title(this);
      });
    });

    $('#dataTable').css('width','100%');
    $("#dataTable_info").parent().closest('div').css('display', 'none');
    $("#dataTable_paginate").parent().closest('div').attr('class', 'col-sm-12');
    $("#dataTable_paginate").css('float','left');
    
    /*Reset filter*/
    $('#reset').on("click", function(){
      $('#task-table-filter').val('');
      oTable.search('').draw();
    });
  });





  /*hide/show html tags*/ 
  if($('#accordion2_1').hasClass('panel-collapse collapse in')) 
  {
    $('#list-title').css('display','none');
  }

  $('#browse').on("click", function(){
    if($('#browse').hasClass('accordion-toggle collapsed')) 
    {
      $('#dataTable-length').show();
      $('#list-title').hide();
    }
    else
    {
      $('#dataTable-length').hide();
      $('#list-title').show();
    }
  });

  $('#collapse-all').on("click", function(){
    $('#collapse-all').css('display', 'none')
    $('#minimize-all').show();
    $('.accordion-toggle').addClass('collapsed');
    $('.accordion-toggle').attr('aria-expanded','false');
    $('.panel-collapse').attr('aria-expanded','false');
    $('.panel-collapse').removeClass('in');

    $('#or-length').css('display','none');
    $('#itemdb-length').css('display','none');
    $('#admin-length').css('display','none');
    $('#or-search-panel').css('display','none');
    $('#itemdb-search-panel').css('display','none');
    $('#admin-search-panel').css('display','none');
  });

  $('#minimize-all').on("click", function(){
    $('#minimize-all').css('display', 'none')
    $('#collapse-all').show();
    $('.accordion-toggle').removeClass('collapsed');
    $('.accordion-toggle').attr('aria-expanded','true');
    $('.panel-collapse').attr('aria-expanded','true');
    $('.panel-collapse').addClass('in');

    $('#or-length').show();
    $('#itemdb-length').show();
    $('#admin-length').show();
    $('#or-search-panel').show();
    $('#itemdb-search-panel').show();
    $('#admin-search-panel').show();
  });


});