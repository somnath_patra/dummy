

var upcReview = {
  'get_upc_title' : function(pointer){
  	$('.get-upc-title').hide();
  	$('.get-tags').hide();
  	$('.get-upc').hide();
  	$('.get-retailer').hide();
  	var url = BASE_URL + 'admin/upc-review/getUpcTitle';
  	var id = pointer.getAttribute('data-id');
  	
  	$('#custom-loader').show();						/*show loader*/
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		   
		    /*Append Upc Title Details*/
		    $('.get-upc-title').html(response);
		    $('.breadcrumb').html('<li class="active"><a href="http://127.0.0.1:8000/admin"><i class="voyager-boat"></i> Dashboard</a></li><li><a href="http://127.0.0.1:8000/admin/review-upc"> Review Upc</a></li><li>'+$('.upc-title').html()+'</li>');

			/*GET upc title*/
			var upc_title = $('.upc-title').html();

		    /*Change url for tags view*/
			url = url.replace("getUpcTitle", "getTags");
			/*call get_tags*/ 
			upcReview.get_tags(url,upc_title);

		  }
		});
	},
	'get_tags' : function(url,upc_title){


		$.ajax({
		  url: url,
		  type:'POST',
		  data: {upc_title : upc_title},
		  success:function(response){

		  	/*Change url for Mapping view*/
				url = url.replace("getTags", "getUpc");
				/*call get_tags*/ 
				upcReview.get_upc(url,upc_title);

		  	$('.get-tags').html(response);
		  }
		});
	},
	'get_upc' : function(url,upc_title){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {upc_title : upc_title},
		  success:function(response){
		  	
		  	/*Change url for Mapping view*/
				url = url.replace("getUpc", "getRetailer");
				/*call get_tags*/ 
				upcReview.get_retailer(url,upc_title);

		  	$('.get-upc').html(response);
		  }
		});
	},
	'get_retailer' : function(url,upc_title){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {upc_title : upc_title},
		  success:function(response){
		  	
		  	$('.get-retailer').html(response);
		  },
		  complete: function(){
	        $('#custom-loader').hide();			/*loading complete hide loader*/
	      	$('.get-upc-title').show();
		  	$('.get-tags').show();
		  	$('.get-upc').show();
		  	$('.get-retailer').show();
	      }
		});
	},
	'company_map_delete' : function(pointer){
		var id = pointer.getAttribute('data-id');
		var url = pointer.getAttribute('data-url');
		$('#delete_modal').modal('show');
		
		$('#confirm_delete').click(function(){
			$('#delete_modal').modal('hide');
			
			$.ajax({
			  url: url,
			  type:'POST',
			  data: {id : id},
			  success:function(response){
			  	toastr.success("Successfully Deleted Gis Company Manufacturer Mapper");
			  	$('#tr_'+id).fadeOut('slow');
			  }
			});
		});
	},
	'selected_tags': function(id){
		if($("#"+id+"-override").val() != '')
		{
			$("#"+id+"-append span").each(function(index, elem){
			  if($("#"+id+"-override").val() == $(this).find("span").text())
			  {
			    $("#"+id+"-append span a").html('x');
			    $(this).find("a").html('');
			  }
			});

			$("#pre-"+id+"-tag span").each(function(index, elem){
			  if($("#"+id+"-override").val() == $(this).text())
			  {
			    $("#"+id+"-append span a").html('x');
			  }
			});
		}
	},
    'update_admin' :function(){
    	var data = new Array();

    	$('.admin-data').each(function(){
    		data.push({value:$(this).val(), index:$(this).attr("data-name"), upc:$.trim($("#main_retailer_upc").text())});
    	});
    	
    	var d = JSON.stringify(data);

		$.ajax({
		  url: BASE_URL + 'admin/upc-review/updateAdmin',
		  type:'POST',
		  data: {data : d},
		  success:function(response){
		  	toastr.success("Successfully Updated Upc Review");

		  }
		});
    }
};