$( document ).ready(function() {

  $('.get-details').click(function(){
  	var url = this.getAttribute('data-url');
  	var id = this.getAttribute('data-id');
  	company.get_details(url,id);
  });

  /*Ajax loader*/
  $(document).ajaxStart(function(){
  	$("#voyager-loader").css("display", "block");
  });

  $(document).ajaxComplete(function(){
  	$("#voyager-loader").css("display", "none");
  });
  /*Ajax loader*/

});


var company = {
  'get_details' : function(url,id){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		  	$('.custom-active').css('background-color','#fff');
		  	$('#custom-active_'+id).css('background-color','#f5f5f5');

		    /*Change url for Mapping view*/
				url = url.replace("getDetails", "getMappings");
				/*call get_mappings*/ 
				company.get_mappings(url,id);

		    /*Append Company Details*/
		    $('.company-details').html(response);
		  }
		});
	},
	'get_mappings' : function(url,id){
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		  	$('.company-manufacturer-mapping').html(response);
		  }
		});
	},
	'company_map_delete' : function(pointer){
		var id = pointer.getAttribute('data-id');
		var url = pointer.getAttribute('data-url');
		var token = pointer.getAttribute("data-token");
		$('#delete_modal').modal('show');
		
		$('#confirm_delete').click(function(){
			$('#delete_modal').modal('hide');
			
			$.ajax({
			  url: url,
			  type:'DELETE',
			  data: { /*"_token": token*/ },
			  success:function(response){
			  	toastr.success("Successfully Deleted Gis Company Manufacturer Mapper");
			  	$('#tr_'+id).fadeOut('slow');
			  }
			});
		});
	}
};