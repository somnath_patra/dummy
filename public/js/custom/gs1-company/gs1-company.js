$( document ).ready(function() {
	/*hide/show html tags*/ 
	if($('#accordion2_1').hasClass('panel-collapse collapse in')) 
	{
	$('#list-title').css('display','none');
	}  	
});


var gs1Company = {
  'get_company_details' : function(pointer){
  	$('.get-company-details').hide();
  	$('.get-mapping').hide();
  	var url = BASE_URL + 'admin/gs1-company/getCompanyDetails';
  	var id = pointer.getAttribute('data-id');
  	var name = pointer.getAttribute('data-name');
  	
  	$('#custom-loader').show();		/*show loader*/
		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		   
		    /*Append Upc Title Details*/
		    $('.get-company-details').html(response);
		    $('.breadcrumb').html('<li class="active"><a href="http://127.0.0.1:8000/admin"><i class="voyager-boat"></i> Dashboard</a></li><li><a href="http://127.0.0.1:8000/admin/gs1-company"> Gs1 Company</a></li><li>'+name+'</li>');

		    /*Change url for tags view*/
			url = url.replace("getCompanyDetails", "getCompanyManufacturerMap");
			gs1Company.gs1_company_manufacturer_map(url,id);

		  }
		});
	},
	'gs1_company_manufacturer_map' : function(url,id){

		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		  	$('.get-mapping').html(response)
		  },
		  complete: function(){
	        $('#custom-loader').hide();			
	      	$('.get-company-details').show();
	      	$('.get-mapping').show();
	      }
		});
	},
	'add_mapping' : function(pointer){

		$(pointer).attr('disabled',true);
		$('.add-hidden').show();
		$('.add').hide();

		var id = pointer.getAttribute('data-id');
		var url = pointer.getAttribute('data-url');
		var manufacturer = $("#activate_tagator2").val();

		if(manufacturer)
		{
			$.ajax({
			  url: url,
			  type:'POST',
			  data: {id : id, manufacturer : manufacturer},
			  success:function(response){
			  	toastr.success("Successfully added Gis Company Manufacturer Mapper");
			  	mappingTable.ajax.reload();
		  		$('#activate_tagator2').val('');
		  		$('#tagator_input').val('');
		  		$(".tagator_placeholder").show();
			  },
			  complete: function(){
			  	$('.add-hidden').hide();	
	        $(pointer).attr('disabled',false);
	        $('.add').show();
		  		$(".tagator_tags").html('');
			  	$('#addModal').modal('hide');
			  }
			});
		}
		else
		{
			$(".error-msg").html('Manufacturer field is required');
	  		$(".error-div").slideDown('slow');
	  		setTimeout(function(){ $(".error-div").slideUp('slow'); }, 2000);
	  		$('.add-hidden').hide();	
	        $(pointer).attr('disabled',false);
	        $('.add').show();
	  		return false;
		}
	},
	'update_gs1_company' : function(pointer){
		//alert('dsad');
		var url = pointer.getAttribute('data-url');

		$(pointer).attr('disabled',true);
		$('.update-hidden').show();
		$('.update').hide();

		var company_legal_name = $('#company_legal_name').val();
		var company_gln = $('#company_gln').val();
		var upc_company_prefix = $('#upc_company_prefix').val();
		var gs1_company_prefix = $('#gs1_company_prefix').val();
		var company_address = $('#company_address').val();
		var country = $('#country').val();
		var state_or_province = $('#state_or_province').val();
		var city = $('#city').val();

		var data_string = {company_legal_name : company_legal_name, company_gln : company_gln, upc_company_prefix : upc_company_prefix, gs1_company_prefix : gs1_company_prefix, company_address : company_address, country : country, state_or_province : state_or_province, city : city}

		$.ajax({
		  url: url,
		  type:'POST',
		  data: data_string,
		  success:function(response){
		  	toastr.success("Successfully Updated Gs1 Company");
		  	var d = JSON.parse(response);
		  	$('#created_at').html(d.creation_date);
		  	$('#updated_at').html(d.last_update_date);
		  },
		  complete: function(){
	        $('.update-hidden').hide();	
	        $(pointer).attr('disabled',false);
	        $('.update').show();	
	      }
		});
	},
	'change_valid_status' : function(pointer){
		var url = pointer.getAttribute('data-url');
		var id = $('#change-status').val();
		$(pointer).attr('disabled','true');

		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		  	toastr.success("Successfully updated company manufacturer mapping valid status.");
		  	mappingTable.ajax.reload();
		  },
		  error: function(response){
		  	toastr.error("Sorry record no found.");
		  	mappingTable.ajax.reload();
		  },
		  complete: function(){
		  	$(pointer).attr('disabled',false);
		  	$('#changeStatus').modal('hide');
		  }
		});
	},
	'delete_mapping' : function(pointer){
		var url = pointer.getAttribute('data-url');
		var id = $('#delete-item').val();
		$(pointer).attr('disabled','true');

		$.ajax({
		  url: url,
		  type:'POST',
		  data: {id : id},
		  success:function(response){
		  	toastr.success("Successfully deleted company manufacturer mapping.");
		  	mappingTable.ajax.reload();
		  },
		  error: function(response){
		  	toastr.error("Sorry record no found.");
		  	mappingTable.ajax.reload();
		  },
		  complete: function(){
		  	$(pointer).attr('disabled',false);
		  	$('#deleteModal').modal('hide');
		  }
		});
	},
	'toggle_mappingTable_sl': function(){
		if($('#mapping-browse').hasClass('accordion-toggle collapsed')) 
		{
			$('#mapping-length').show();
			$('#mapping-search-panel').show();
		}
		else
		{
			$('#mapping-length').css('display','none');
			$('#mapping-search-panel').css('display','none');
		}
	},
	'toggle_dataTable_sl': function(){
		if($('#browse').hasClass('accordion-toggle collapsed')) 
        {
          $('#dataTable-length').show();
          $('#list-title').hide();
        }
        else
        {
          $('#dataTable-length').hide();
          $('#list-title').show();
        }
	},
	'collapse_all': function(){
		$('#collapse-all').css('display', 'none')
	    $('#minimize-all').show();
	    $('.accordion-toggle').addClass('collapsed');
	    $('.accordion-toggle').attr('aria-expanded','false');
	    $('.panel-collapse').attr('aria-expanded','false');
	    $('.panel-collapse').removeClass('in');

	    $('#mapping-length').css('display','none');
	    $('#itemdb-length').css('display','none');
	    $('#admin-length').css('display','none');
	    $('#mapping-search-panel').css('display','none');
	    $('#itemdb-search-panel').css('display','none');
	    $('#admin-search-panel').css('display','none');
	},
	'minimize_all': function(){
		$('#minimize-all').css('display', 'none')
        $('#collapse-all').show();
        $('.accordion-toggle').removeClass('collapsed');
        $('.accordion-toggle').attr('aria-expanded','true');
        $('.panel-collapse').attr('aria-expanded','true');
        $('.panel-collapse').addClass('in');

        $('#mapping-length').show();
        $('#itemdb-length').show();
        $('#admin-length').show();
        $('#mapping-search-panel').show();
        $('#itemdb-search-panel').show();
        $('#admin-search-panel').show();
	},
	'get_manufacturer': function(pointer){
		var url = pointer.getAttribute('data-url');
		$.ajax({
		  url: url,
		  type:'get',
		  /*data: {id : id},*/
		  success:function(response){
		  	var d = JSON.parse(response);
		  	$('#activate_tagator2').tagator('autocomplete', d);
		  }
		});
	}
};