$(document).ready(function () {
  /*Server side Datatable*/
  $(function() {
    $('#dataTable').DataTable({
      processing: false,
      serverSide: true,
      ajax: BASE_URL + 'admin/gs1-company/getCompanyListAjaxDataTable',
      columns: [
          { data: 'company_legal_name', name: 'company_legal_name',
          "render": function(data,type,row,meta) {  
              var a = '<a class="get-details" data-name="'+row.company_legal_name+'" data-id="'+row.gs1_company_prefix+'" title="View Details">' + row.company_legal_name +'</a>'; 
              return a;
            }/*,
            orderable:false*/
          }
      ],
      "initComplete": function(settings, json) {
        $("#dataTable thead tr th").css("width","100%");
        $("#dataTable tbody").find("tr:first a").trigger('click');
      }
    });

    oTable = $('#dataTable').DataTable();   
    $('#task-table-filter').keyup(function(){
      oTable.search($(this).val()).draw();
    });

    $('#dataTable-length').val(oTable.page.len());
    $('#dataTable-length').change( function() { 
      oTable.page.len( $(this).val() ).draw();
    });

    oTable.on( 'draw', function () {
      
      $(".get-details").on("click", function(){
        $("tr").removeClass("active");
        $(this).parent().closest("tr").addClass("active");
        gs1Company.get_company_details(this);
      });
    });

    $('#dataTable').css('width','100%');
    $("#dataTable_info").parent().closest('div').css('display', 'none');
    $("#dataTable_paginate").parent().closest('div').attr('class', 'col-sm-12');
    $("#dataTable_paginate").css('float','left');

    /*Reset filter*/
    $('#reset').on("click", function(){
      $('#task-table-filter').val('');
      oTable.search('').draw();
    });
  });
  
  

});