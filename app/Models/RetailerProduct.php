<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class RetailerProduct
 * @package App\Models
 * @version June 15, 2018, 1:06 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property integer retailer_id
 * @property string sku
 * @property string name
 * @property string retailer_upc
 * @property string model_number
 * @property string manufacturer
 * @property string long_description
 * @property string short_description
 * @property decimal regular_price
 * @property string specifications
 * @property string features
 * @property string isbn
 * @property string color
 * @property string url
 * @property string mobile_url
 * @property string thumbnail_image_url
 * @property string medium_image_url
 * @property string large_image_url
 * @property string customer_review_average
 * @property string customer_review_count
 * @property string customer_review_url
 * @property string|\Carbon\Carbon date_released
 * @property string year_released
 * @property string category_path
 * @property string product_condition
 * @property string previous_sku
 * @property string next_sku
 * @property string checksum
 * @property boolean marked_for_updation
 * @property boolean end_of_life_flag
 * @property string datasource_url
 * @property integer created_by
 * @property string|\Carbon\Carbon creation_date
 * @property integer updated_by
 * @property string|\Carbon\Carbon last_update_date
 * @property string capacity
 * @property string capacity_variants
 * @property string color_variants
 * @property string size
 * @property string size_variants
 */
class RetailerProduct extends Model
{

    public $table = 'retailer_product';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'retailer_product_id';

    /*public $fillable = [
        'retailer_id',
        'sku',
        'name',
        'retailer_upc',
        'model_number',
        'manufacturer',
        'long_description',
        'short_description',
        'regular_price',
        'specifications',
        'features',
        'isbn',
        'color',
        'url',
        'mobile_url',
        'thumbnail_image_url',
        'medium_image_url',
        'large_image_url',
        'customer_review_average',
        'customer_review_count',
        'customer_review_url',
        'date_released',
        'year_released',
        'category_path',
        'product_condition',
        'previous_sku',
        'next_sku',
        'checksum',
        'marked_for_updation',
        'end_of_life_flag',
        'datasource_url',
        'created_by',
        'creation_date',
        'updated_by',
        'last_update_date',
        'capacity',
        'capacity_variants',
        'color_variants',
        'size',
        'size_variants'
    ];*/

    
}
