<?php

namespace App\Models;

use Eloquent as Model;
use DB;
use Schema;
use Carbon\Carbon;
/**
 * Class Retailer
 * @package App\Models
 * @version April 30, 2018, 6:10 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string retailer_code
 * @property string retailer_name
 * @property string retailer_region
 * @property boolean enabled_flag
 * @property string logo_small_url
 * @property string logo_medium_url
 * @property string logo_large_url
 * @property string secret_key_list
 * @property string robots_url
 * @property string sitemap_product_url_list
 * @property string|\Carbon\Carbon sitemap_product_url_lastmod
 * @property string sitemap_category_url_list
 * @property string|\Carbon\Carbon sitemap_category_url_lastmod
 * @property boolean api_enabled
 * @property string api_base_uri
 * @property integer api_limit_per_sec
 * @property integer api_limit_per_min
 * @property integer api_limit_per_hour
 * @property integer api_limit_per_day
 * @property time api_limit_reset_time
 * @property boolean crawl_enabled
 * @property integer crawl_limit_per_sec
 * @property integer crawl_limit_per_min
 * @property integer crawl_limit_per_hour
 * @property integer crawl_limit_per_day
 * @property string retailer_url
 * @property string categories_enabled
 * @property time retailer_api_reset_time
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class Retailer extends Model
{

    public $table = 'retailer';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'retailer_id';

    public function convertToMin($tableName, $schedule_min)
    {
        if(Schema::hasTable($tableName))
        {
            $table = DB::table($tableName)->select('last_update_date')->orderBy('last_update_date', 'DESC')->take(1)->first();

            if($table)
            {
                $seconds = strtotime($table->last_update_date);
                $minute = $seconds/60;

                $current_time = strtotime(date('Y-m-d H:i:s'));

                $data = $minute >= (($current_time/60) - $schedule_min);

                if($data)
                {
                    $data = array(
                        'updated_at' => $table->last_update_date,
                        'response_code' => 1
                    );
                    return ($data);
                }
                else
                {
                    $data = array(
                        'updated_at' => $table->last_update_date,
                        'response_code' => 0
                    );
                    return ($data);
                }
            }
            else
            {
                $data = array(
                    'updated_at' => '',
                    'response_code' => 4
                );
                return ($data);
            }

        }
        else
        {
            $data = array(
                'updated_at' => '',
                'response_code' => 3
            );
            return ($data);
        }
    }

    /*public $fillable = [
        'retailer_code',
        'retailer_name',
        'retailer_region',
        'enabled_flag',
        'logo_small_url',
        'logo_medium_url',
        'logo_large_url',
        'secret_key_list',
        'robots_url',
        'sitemap_product_url_list',
        'sitemap_product_url_lastmod',
        'sitemap_category_url_list',
        'sitemap_category_url_lastmod',
        'api_enabled',
        'api_base_uri',
        'api_limit_per_sec',
        'api_limit_per_min',
        'api_limit_per_hour',
        'api_limit_per_day',
        'api_limit_reset_time',
        'crawl_enabled',
        'crawl_limit_per_sec',
        'crawl_limit_per_min',
        'crawl_limit_per_hour',
        'crawl_limit_per_day',
        'retailer_url',
        'categories_enabled',
        'retailer_api_reset_time',
        'creation_date',
        'last_update_date'
    ];*/

    
}