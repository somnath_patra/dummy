<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class RetailerXpath
 * @package App\Models
 * @version June 18, 2018, 8:44 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property integer retailer_id
 * @property string field
 * @property string xpath
 * @property string page_type
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class RetailerXpath extends Model
{

    public $table = 'retailer_xpath';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'xpath_id';

    public function retailer()
    {       
        return $this->hasOne('App\Models\Retailer','retailer_id','retailer_id');
    }

    
}
