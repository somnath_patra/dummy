<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ReviewUpc
 * @package App\Models
 * @version June 15, 2018, 11:46 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string admin_upc_override
 * @property string admin_model_number_override
 * @property string admin_name_override
 * @property string admin_manufacturer_override
 * @property integer created_by
 * @property string|\Carbon\Carbon creation_date
 * @property integer updated_by
 * @property string|\Carbon\Carbon last_update_date
 * @property string admin_analysis_log
 * @property string admin_category_tags_append
 * @property string admin_gs1_analysis_log
 * @property string admin_gs1_company_prefix
 * @property string|\Carbon\Carbon admin_gs1_last_update_date
 * @property string admin_gs1_upc_company_prefix
 * @property boolean admin_gs1_validated_flag
 * @property string|\Carbon\Carbon admin_last_update_date
 * @property string admin_name_tags_append
 * @property string admin_manufacturer_tags_append
 * @property string admin_model_tags_append
 * @property integer admin_updated_by
 * @property string itemdb_analysis_log
 * @property string itemdb_gs1_analysis_log
 * @property string itemdb_gs1_company_prefix
 * @property string|\Carbon\Carbon itemdb_gs1_last_update_date
 * @property string itemdb_gs1_upc_company_prefix
 * @property boolean itemdb_gs1_validated_flag
 * @property string|\Carbon\Carbon itemdb_last_update_date
 * @property string itemdb_upc_derived
 * @property string itemdb_upc_tag
 * @property string retailer_gs1_analysis_log
 * @property string retailer_gs1_company_prefix
 * @property string|\Carbon\Carbon retailer_gs1_last_update_date
 * @property string retailer_gs1_upc_company_prefix
 * @property boolean retailer_gs1_validated_flag
 */
class ReviewUpc extends Model
{

    public $table = 'review_upc';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'upc_review_id';

    /*public $fillable = [
        'admin_upc_override',
        'admin_model_number_override',
        'admin_name_override',
        'admin_manufacturer_override',
        'created_by',
        'creation_date',
        'updated_by',
        'last_update_date',
        'admin_analysis_log',
        'admin_category_tags_append',
        'admin_gs1_analysis_log',
        'admin_gs1_company_prefix',
        'admin_gs1_last_update_date',
        'admin_gs1_upc_company_prefix',
        'admin_gs1_validated_flag',
        'admin_last_update_date',
        'admin_name_tags_append',
        'admin_manufacturer_tags_append',
        'admin_model_tags_append',
        'admin_updated_by',
        'itemdb_analysis_log',
        'itemdb_gs1_analysis_log',
        'itemdb_gs1_company_prefix',
        'itemdb_gs1_last_update_date',
        'itemdb_gs1_upc_company_prefix',
        'itemdb_gs1_validated_flag',
        'itemdb_last_update_date',
        'itemdb_upc_derived',
        'itemdb_upc_tag',
        'retailer_gs1_analysis_log',
        'retailer_gs1_company_prefix',
        'retailer_gs1_last_update_date',
        'retailer_gs1_upc_company_prefix',
        'retailer_gs1_validated_flag'
    ];*/

    
}
