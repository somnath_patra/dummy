<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Manufacturer
 * @package App\Models
 * @version April 28, 2018, 5:11 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string manufacturer_name
 * @property boolean enabled_flag
 * @property string manufacturer_url
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class Manufacturer extends Model
{

    public $table = 'manufacturers';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'manufacturer_id';

    /*public $fillable = [
        'manufacturer_name',
        'enabled_flag',
        'manufacturer_url',
        'creation_date',
        'last_update_date'
    ];*/

    
}
