<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Category
 * @package App\Models
 * @version May 4, 2018, 8:55 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string|\Carbon\Carbon creation_date
 * @property string category_name
 * @property string category_path
 * @property string parent_category_id
 * @property boolean enabled_flag
 * @property boolean search_flag
 * @property boolean has_products_flag
 * @property integer depth
 * @property string image_url
 * @property string uri
 * @property string|\Carbon\Carbon last_update_date
 */
class Category extends Model
{

    public $table = 'category';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'category_id';

    /*public $fillable = [
        'creation_date',
        'category_name',
        'category_path',
        'parent_category_id',
        'enabled_flag',
        'search_flag',
        'has_products_flag',
        'depth',
        'image_url',
        'uri',
        'last_update_date'
    ];*/

    
}
