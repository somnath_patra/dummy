<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class CompanyManufacturerMap
 * @package App\Models
 * @version April 28, 2018, 4:42 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property integer gis_company_id
 * @property integer manufacturer_id
 * @property string reference_manufacturer
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class CompanyManufacturerMap extends Model
{

    public $table = 'gis_company_manufacturer_mapper';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'gs1_manufacturer_mapper_id';

    /*public $fillable = [
        'gis_company_id',
        'manufacturer_id',
        'reference_manufacturer',
        'creation_date',
        'last_update_date'
    ];*/

    
}
