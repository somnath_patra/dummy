<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class RewardMembership
 * @package App\Models
 * @version June 18, 2018, 6:52 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string reward_membership_name
 * @property decimal yearly_minimum_spend_required
 * @property string description
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class RewardMembership extends Model
{

    public $table = 'reward_membership';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'reward_membership_id';

    /*public $fillable = [
        'reward_membership_name',
        'yearly_minimum_spend_required',
        'description',
        'creation_date',
        'last_update_date'
    ];*/

    
}
