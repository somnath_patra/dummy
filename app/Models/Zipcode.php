<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Zipcode
 * @package App\Models
 * @version June 27, 2018, 10:59 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string zipcode_type
 * @property string city
 * @property string state
 * @property string location_type
 * @property float latitude
 * @property float longitude
 * @property string location
 * @property string decommissioned
 * @property string tax_returns_filed
 * @property string estimated_population
 * @property string total_wages
 * @property string bestbuy_sync_flag
 * @property string walmart_sync_flag
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class Zipcode extends Model
{

    public $table = 'zipcodes';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'zipcode_id';
    public $incrementing = false;

    /*public $fillable = [
        'zipcode_type',
        'city',
        'state',
        'location_type',
        'latitude',
        'longitude',
        'location',
        'decommissioned',
        'tax_returns_filed',
        'estimated_population',
        'total_wages',
        'bestbuy_sync_flag',
        'walmart_sync_flag',
        'creation_date',
        'last_update_date'
    ];*/

    
}
