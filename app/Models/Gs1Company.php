<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Gs1Company
 * @package App\Models
 * @version June 1, 2018, 5:36 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string upc_company_prefix
 * @property string company_legal_name
 * @property string company_gln
 * @property string company_address
 * @property string company_address_1
 * @property string company_address_2
 * @property string company_address_3
 * @property string company_address_4
 * @property string city
 * @property string state_or_province
 * @property string zipcode
 * @property string country
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class Gs1Company extends Model
{

    public $table = 'gs1_company';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'gs1_company_prefix';

    /*public $fillable = [
        'upc_company_prefix',
        'company_legal_name',
        'company_gln',
        'company_address',
        'company_address_1',
        'company_address_2',
        'company_address_3',
        'company_address_4',
        'city',
        'state_or_province',
        'zipcode',
        'country',
        'creation_date',
        'last_update_date'
    ];*/

    
}
