<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Product
 * @package App\Models
 * @version May 2, 2018, 6:51 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string source_retailer_sku
 * @property integer source_retailer_id
 * @property string name
 * @property string long_description
 * @property string short_description
 * @property string model_number
 * @property integer manufacturer_id
 * @property decimal regular_price
 * @property string specifications
 * @property string features
 * @property string upc
 * @property string isbn
 * @property string color
 * @property string url
 * @property string mobile_url
 * @property string thumbnail_image_url
 * @property string medium_image_url
 * @property string large_image_url
 * @property string customer_review_average
 * @property string customer_review_count
 * @property string expert_review_average
 * @property string expert_review_count
 * @property string|\Carbon\Carbon date_released
 * @property string year_released
 * @property string category_path
 * @property string checksum
 * @property boolean marked_for_updation
 * @property boolean end_of_life_flag
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class Product extends Model
{

    public $table = 'product';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'product_id';

    /*public $fillable = [
        'source_retailer_sku',
        'source_retailer_id',
        'name',
        'long_description',
        'short_description',
        'model_number',
        'manufacturer_id',
        'regular_price',
        'specifications',
        'features',
        'upc',
        'isbn',
        'color',
        'url',
        'mobile_url',
        'thumbnail_image_url',
        'medium_image_url',
        'large_image_url',
        'customer_review_average',
        'customer_review_count',
        'expert_review_average',
        'expert_review_count',
        'date_released',
        'year_released',
        'category_path',
        'checksum',
        'marked_for_updation',
        'end_of_life_flag',
        'creation_date',
        'last_update_date'
    ];*/

    
}
