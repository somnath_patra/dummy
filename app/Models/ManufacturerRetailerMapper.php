<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ManufacturerRetailerMapper
 * @package App\Models
 * @version April 30, 2018, 6:20 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string retailer_manufacturer
 * @property integer master_manufacturer_id
 * @property boolean reviewed_flag
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class ManufacturerRetailerMapper extends Model
{

    public $table = 'manufacturer_retailer_mapper';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'manufacturer_retailer_mapper_id';

    /*public $fillable = [
        'retailer_manufacturer',
        'master_manufacturer_id',
        'reviewed_flag',
        'creation_date',
        'last_update_date'
    ];*/

    
}
