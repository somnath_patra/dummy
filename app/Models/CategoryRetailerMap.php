<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class CategoryRetailerMap
 * @package App\Models
 * @version May 4, 2018, 8:51 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property integer category_id
 * @property string category_name
 * @property string category_path
 * @property integer retailer_id
 * @property string retailer_name
 * @property string retailer_category_id
 * @property string retailer_category_name
 * @property string retailer_category_path
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class CategoryRetailerMap extends Model
{

    public $table = 'category_retailer_mapper';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'retailer_category_mapper_id';

    /*public $fillable = [
        'category_id',
        'category_name',
        'category_path',
        'retailer_id',
        'retailer_name',
        'retailer_category_id',
        'retailer_category_name',
        'retailer_category_path',
        'creation_date',
        'last_update_date'
    ];*/

    
}
