<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class DealType
 * @package App\Models
 * @version June 18, 2018, 7:22 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property string deal_type
 * @property string deal_type_description
 * @property string small_image_url
 * @property string medium_image_url
 * @property string large_image_url
 * @property string uri
 * @property float percent_cap_low
 * @property float percent_cap_high
 * @property boolean show_in_ui
 * @property boolean enabled_flag
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class DealType extends Model
{

    public $table = 'deal_type';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'deal_type_id';

    /*public $fillable = [
        'deal_type',
        'deal_type_description',
        'small_image_url',
        'medium_image_url',
        'large_image_url',
        'uri',
        'percent_cap_low',
        'percent_cap_high',
        'show_in_ui',
        'enabled_flag',
        'creation_date',
        'last_update_date'
    ];*/

    
}
