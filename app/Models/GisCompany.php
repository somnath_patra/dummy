<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class GisCompany
 * @package App\Models
 * @version April 28, 2018, 4:38 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property integer gis_company_id
 * @property string gs1_company_prefix
 * @property string upc_company_prefix
 * @property string company_legal_name
 * @property string company_gln
 * @property string company_address
 * @property string company_address_1
 * @property string company_address_2
 * @property string company_address_3
 * @property string company_address_4
 * @property string city
 * @property string state_or_province
 * @property string zipcode
 * @property string country
 * @property string|\Carbon\Carbon creation_date
 * @property string|\Carbon\Carbon last_update_date
 */
class GisCompany extends Model
{

    public $table = 'gis_company';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'gis_company_id';

    /*public $fillable = [
        'gis_company_id',
        'gs1_company_prefix',
        'upc_company_prefix',
        'company_legal_name',
        'company_gln',
        'company_address',
        'company_address_1',
        'company_address_2',
        'company_address_3',
        'company_address_4',
        'city',
        'state_or_province',
        'zipcode',
        'country',
        'creation_date',
        'last_update_date'
    ];*/

    
}
