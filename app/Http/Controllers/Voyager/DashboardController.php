<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Illuminate\Support\Facades\Schema;
use Yajra\Datatables\Datatables;
use Response;
use App\Models\Retailer as Retailer;

class DashboardController extends VoyagerBaseController
{

    public function getRetailerAjaxDataTable(Request $request)
    {
        $custom_filter = json_decode($_GET['search']['value']);

        $query = Retailer::select('retailer_name', 'retailer_cron_schedule','retailer_id');

        return Datatables::of($query)
            ->setRowClass('{{ "custom-active" }}')
            ->setRowId('custom-active_{{$retailer_id}}')
            ->filter(function($query) use ($custom_filter) {
                if(is_array($custom_filter)){
                  foreach ($custom_filter as $key) {
                    if($key->value=='true')
                    {
                        $query->where($key->index, '=', true);
                    }
                    else if($key->value=='false')
                    {
                        $query->where($key->index, '=', false);
                    }
                    else
                    {
                        $query->get();
                    }
                  }
                }
                else if(empty($_GET['search']['value'])) {
                  $query->get();
                }
                else { 
                  $query->where('retailer_name', 'like', "%".$_GET['search']['value']."%");
                }
            })
            ->addColumn('action', function($query) {

                $cron_data = json_decode($query->retailer_cron_schedule);
                $obj = (object)$cron_data;
                $arr = (array)$obj;

                $data = '<table class="table table-bordered" style="margin:5px 0px"><tbody><tr>';

                if(empty($arr)){
                    $data .= 'Retailer cron schedule data not available';
                }
                else
                {
                    foreach ($arr as $value) {
                        //print_r($value);
                        $retailer = new Retailer;
                        $response = $retailer->convertToMin($value->table, $value->schedule_min);

                        if($response['response_code']==1){
                            $data .= '<td class="alert alert-success cron-data" style="border-left:5px solid #fff;padding: 2px 5px;"><p>'.$value->table.'<br>'.$response['updated_at'].'</p></td>';
                        }
                        if($response['response_code']==0){
                            $data .= '<td class="alert alert-danger cron-data" style="border-left:5px solid #fff;padding: 2px 5px;"><p>'.$value->table.'<br>'.$response['updated_at'].'</p></td>';
                        }
                        if ($response['response_code']==3) {
                            $data .= '<td class="alert alert-warning cron-data" style="border-left:5px solid #fff;padding: 2px 5px;"><p>'.$value->table.' does not exist</p></td>';
                        }
                        if ($response['response_code']==4) {
                            $data .= '<td class="alert alert-default cron-data" style="border-left:5px solid #fff;padding: 2px 5px;"><p>'.$value->table.' data not available</p></td>';
                        }
                    }
                }
                return $data.'</tr></tbody></table>';
            })
            
           ->make(true);
    }

    
}