<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Schema;
use Yajra\Datatables\Datatables;
//use App\Models\ReviewUpc;

class UpcReviewController extends VoyagerBaseController
{

  private $RetailerProduct = 'App\Models\RetailerProduct';
  private $UpcReview = 'App\Models\UpcReview';

  public function getUpcTitle(Request $request)
  {
      $id = $_POST['id'];
      $reviewUpcTitle = $this->UpcReview::
                select('retailer_upc')
              ->where('retailer_upc', '=', $id)->first(); 

      $view = 'vendor.voyager.upc-review.upc-title';

      return Voyager::view($view, compact('reviewUpcTitle','id'));
  } 

  public function getTags(Request $request)
  {
      $upc_title = $_POST['upc_title'];
      $tags = $this->RetailerProduct::
                where('retailer_upc', '=', $upc_title); 

      $append_tags = $this->UpcReview::
                select('retailer_upc','admin_manufacturer_tags_append','admin_model_number_tags_append','admin_name_tags_append','admin_manufacturer_override','admin_model_number_override','admin_name_override')
              ->where('retailer_upc', '=', $upc_title)->first();

      $dataFields = array('name','manufacturer','model_number','category_path','regular_price');

      $view = 'vendor.voyager.upc-review.tags';

      return Voyager::view($view, compact('tags','dataFields','append_tags'));
  }

  public function getUpc(Request $request)
  {
      $upc_title = $_POST['upc_title'];
      $upc = $this->UpcReview::
                where('retailer_upc', '=', $upc_title)->first(); 

      $view = 'vendor.voyager.upc-review.upc';

      return Voyager::view($view, compact('upc'));
  } 

  public function getRetailer(Request $request)
  {
      $upc_title = $_POST['upc_title'];

      $upc = $this->UpcReview::
                where('retailer_upc', '=', $upc_title)->first(); 

      $view = 'vendor.voyager.upc-review.retailer';

      return Voyager::view($view, compact('upc'));
  }

  public function addMnfTags(Request $request)
  {
      $retailer_upc = $this->UpcReview::select('retailer_upc','admin_manufacturer_tags_append')
              ->where('retailer_upc', '=', $request->upc)->first(); 

      if(empty($retailer_upc->retailer_upc))
      {
        $data = json_encode(explode(",", $request->tags));

        $this->UpcReview::
            insert([
                'retailer_upc' => $request->upc,
                'admin_manufacturer_tags_append' => $data,
                'creation_date'=>now(),
                'last_update_date'=>now()
            ]);
      }
      else
      {
        $current_data = json_encode(explode(",", $request->tags));
        
        $this->UpcReview::
              where('retailer_upc','=',$request->upc)
            ->update([
                'last_update_date'=>now(),
                'admin_manufacturer_tags_append' => $current_data
            ]);
      }
  } 

  public function addModelTags(Request $request)
  {
    $retailer_upc = $this->UpcReview::select('retailer_upc','admin_model_tags_append')
              ->where('retailer_upc', '=', $request->upc)->first(); 

      if(empty($retailer_upc->retailer_upc))
      {
        $data = json_encode(explode(",", $request->tags));

        $this->UpcReview::
            insert([
                'retailer_upc' => $request->upc,
                'admin_model_tags_append' => $data,
                'creation_date'=>now(),
                'last_update_date'=>now()
            ]);
      }
      else
      {
        $current_data = json_encode(explode(",", $request->tags));
        
        $this->UpcReview::
              where('retailer_upc','=',$request->upc)
            ->update([
                'last_update_date'=>now(),
                'admin_model_tags_append' => $current_data
            ]);
      }
  }

  public function addNameTags(Request $request)
  {
    $retailer_upc = $this->UpcReview::select('retailer_upc','admin_name_tags_append')
              ->where('retailer_upc', '=', $request->upc)->first(); 

      if(empty($retailer_upc->retailer_upc))
      {
        $data = json_encode(explode(",", $request->tags));

        $this->UpcReview::
              insert([
                'retailer_upc' => $request->upc,
                'admin_name_tags_append' => $data,
                'creation_date'=>now(),
                'last_update_date'=>now()
            ]);
      }
      else
      {
        $current_data = json_encode(explode(",", $request->tags));
        
        $this->UpcReview::
              where('retailer_upc','=',$request->upc)
            ->update([
                'last_update_date'=>now(),
                'admin_name_tags_append' => $current_data
            ]);
      }
  }

  public function mnfOverride(Request $request)
  {
    $mnf_appended_tags = $this->UpcReview::
                select('retailer_upc','admin_manufacturer_tags_append')
              ->where('retailer_upc', '=', $request->upc)->first();

    $tags = $this->RetailerProduct::select('manufacturer')->where('retailer_upc', '=', $request->upc)->get(); 

    $response = array();
    $data = json_decode($mnf_appended_tags->admin_manufacturer_tags_append);

    /*retailer_product tags*/
    if($tags)
    {
      foreach ($tags as $key) {
        if($key->manufacturer)
        {
          $response[] = array("id"=>$key->manufacturer, "text"=>$key->manufacturer);
        }
      }
    }
    /*upc_review tags*/
    if($mnf_appended_tags->admin_manufacturer_tags_append)
    {
      $data = json_decode($mnf_appended_tags->admin_manufacturer_tags_append);
      for ($i=0; $i < count($data); $i++) {
        $response[] = array("id"=>$data[$i], "text"=>$data[$i]);
      }
    }
    echo json_encode($response);
  }

  public function modelOverride(Request $request)
  {
    $mnf_appended_tags = $this->UpcReview::
                select('retailer_upc','admin_model_number_tags_append')
              ->where('retailer_upc', '=', $request->upc)->first();

    $tags = $this->RetailerProduct::select('model_number')->where('retailer_upc', '=', $request->upc)->get(); 

    $response = array();

    /*retailer_product tags*/
    if($tags)
    {
      foreach ($tags as $key) {
        if($key->model_number)
        {
          $response[] = array("id"=>$key->model_number, "text"=>$key->model_number);
        }
      }
    }
    /*upc_review tags*/
    if($mnf_appended_tags->admin_model_tags_append)
    {
      $data = json_decode($mnf_appended_tags->admin_model_tags_append);
      for ($i=0; $i < count($data); $i++) {
        $response[] = array("id"=>$data[$i], "text"=>$data[$i]);
      }
    }
    echo json_encode($response);
  }

  public function nameOverride(Request $request)
  {
    $mnf_appended_tags = $this->UpcReview::
                select('retailer_upc','admin_name_tags_append')
              ->where('retailer_upc', '=', $request->upc)->first();

    $tags = $this->RetailerProduct::select('name')->where('retailer_upc', '=', $request->upc)->get(); 

    $response = array();

    /*retailer_product tags*/
    if($tags)
    {
      foreach ($tags as $key) {
        if($key->name)
        {
          $response[] = array("id"=>$key->name, "text"=>$key->name);
        }
      }
    }
    /*upc_review tags*/
    if($mnf_appended_tags->admin_name_tags_append)
    {
      $data = json_decode($mnf_appended_tags->admin_name_tags_append);
      for ($i=0; $i < count($data); $i++) {
        $response[] = array("id"=>$data[$i], "text"=>$data[$i]);
      }
    }
    echo json_encode($response);
  }

  public function addMnfOverride(Request $request)
  {
    $retailer_upc = $this->UpcReview::select('retailer_upc','admin_manufacturer_override')
              ->where('retailer_upc', '=', $request->upc)->first(); 

    if(empty($retailer_upc->retailer_upc))
    {
      $this->UpcReview::
          insert([
              'admin_manufacturer_override' => $request->mnfOverride,
              'creation_date'=>now(),
              'last_update_date'=>now()
          ]);
    }
    else
    {
      $this->UpcReview::
            where('retailer_upc','=',$request->upc)
          ->orWhere('admin_manufacturer_override','=',$retailer_upc->retailer_upc)
          ->update([
              'last_update_date'=>now(),
              'admin_manufacturer_override' => $request->mnfOverride
          ]);
    }
  }

  public function addModelOverride(Request $request)
  {
    $retailer_upc = $this->UpcReview::select('retailer_upc','admin_model_number_override')
              ->where('retailer_upc', '=', $request->upc)->first(); 

    if(empty($retailer_upc->retailer_upc))
    {
      $this->UpcReview::
            insert([
              'admin_model_number_override' => $request->modelOverride,
              'creation_date'=>now(),
              'last_update_date'=>now()
            ]);
    }
    else
    {
      $this->UpcReview::
            where('retailer_upc','=',$request->upc)
          ->orWhere('admin_model_number_override','=',$retailer_upc->retailer_upc)
          ->update([
              'last_update_date'=>now(),
              'admin_model_number_override' => $request->modelOverride
          ]);
    }
  }

  public function addNameOverride(Request $request)
  {
    $retailer_upc = $this->UpcReview::select('retailer_upc','admin_name_override')
              ->where('retailer_upc', '=', $request->upc)->first(); 

    if(empty($retailer_upc->retailer_upc))
    {
      $this->UpcReview::
            insert([
              'admin_name_override' => $request->nameOverride,
              'creation_date'=>now(),
              'last_update_date'=>now()
          ]);
    }
    else
    {
      $this->UpcReview::
            where('retailer_upc','=',$request->upc)
          ->orWhere('admin_name_override','=',$retailer_upc->retailer_upc)
          ->update([
              'last_update_date'=>now(),
              'admin_name_override' => $request->nameOverride
          ]);
    }
  }

  public function MnfDuplicationCheck(Request $request)
  {
    $tags = $this->RetailerProduct::
          select('manufacturer')
        ->where('manufacturer', '=', $request->tag)
        ->get();

    if(count($tags))
    {
      echo 1;exit;
    }
    else
    {
      echo 0;exit;
    }
  }

  public function ModelDuplicationCheck(Request $request)
  {
    $tags = $this->RetailerProduct::
          select('model_number')
        ->where('model_number', '=', $request->tag)
        ->get();

    if(count($tags))
    {
      echo 1;exit;
    }
    else
    {
      echo 0;exit;
    }
  }

  public function NameDuplicationCheck(Request $request)
  {
    $tags = $this->RetailerProduct::
          select('name')
        ->where('name', '=', $request->tag)
        ->get();
    if(count($tags))
    {
      echo 1;exit;
    }
    else
    {
      echo 0;exit;
    }
  }

  public function objectToArray($d) 
  {
    if (is_object($d)) {
        $data = array();

        foreach ($d as $key) {
          $data[] = $key;
        }
        return $data;
    }
    else {
        // Return array
        return $d;
    }
  }

  /**
   * Process datatables ajax request.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function getOrgRetailerAjaxDataTable(Request $request)
  {
      $query = $this->RetailerProduct::
                    select('name','manufacturer','category_path','regular_price','model_number')
                  ->where('retailer_upc','=',$_GET['upc']);
      return Datatables::of($query)
          ->make(true);
  }

  public function getItemdbRetailerAjaxDataTable(Request $request)
  {
      $query = $this->RetailerProduct::
                    select('name','manufacturer','category_path','regular_price','model_number')
                  ->where('retailer_upc','=',$_GET['upc']);
      return Datatables::of($query)
          ->make(true);
  }

  public function getAdminRetailerAjaxDataTable(Request $request)
  {
      $query = $this->RetailerProduct::
                    select('name','manufacturer','category_path','regular_price','model_number')
                  ->where('retailer_upc','=',$_GET['upc']);
      return Datatables::of($query)
          ->make(true);
  }

  public function getUpcListAjaxDataTable(Request $request)
  {
    $custom_filter = json_decode($_GET['search']['value']);

    $query = $this->UpcReview::select('retailer_upc');

    return Datatables::of($query)
        ->setRowClass('{{ "custom-active" }}')
        ->make(true);
  }

  public function updateAdmin(Request $request)
  {

    $data = json_decode($_POST['data']);

    foreach ($data as $key) {
      
      if($key->value=='-')
      {
        $value = 'NULL';
      }
      else
      {
        $value = $key->value;
      }
      $query = $this->UpcReview::where('retailer_upc','=', $key->upc)->update([ $key->index => $value, 'last_update_date'=>date('Y-m-d H:i:s') ]);
    }
      
  }

  //***************************************
  //                _____
  //               |  __ \
  //               | |  | |
  //               | |  | |
  //               | |__| |
  //               |_____/
  //
  //         Delete an item BREA(D)
  //
  //****************************************

 /* public function companyMapDelete(Request $request)
  {
      $slug = 'gis_company_manufacturer_mapper';
      $id = $_POST['id'];

      DB::table('gis_company_manufacturer_mapper')->where('gs1_manufacturer_mapper_id', '=', $id)->delete();

      return 1;
  } */
}