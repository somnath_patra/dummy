<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Illuminate\Support\Facades\Schema;
use Yajra\Datatables\Datatables;
use Response;

class RetailerController extends VoyagerBaseController
{
    private $xpath = 'App\Models\RetailerXpath';
    private $Retailer = 'App\Models\Retailer';

    public function getCronTableCount(Request $request)
    {
        $cron_data = json_decode($_POST['data_table']);

        $view = '';

        if(!empty($cron_data))
        {
            $response = array();
            foreach ($cron_data as $key) {

                if(Schema::hasTable($key->table))
                {
                    $count = DB::table($key->table)->count();
                    $table = $key->table;
                    $count = $count;
                    $view = 'vendor.voyager.retailer.cron_table_count';
                }
                else
                {
                    $table = 0;
                    $count = 0;
                    $view = 'vendor.voyager.retailer.cron_table_count';
                }
                $tableData[] =$table;
                $countData[] =$count;
                $response['table'] = $tableData;
                $response['count'] = $countData;
            }
            return Voyager::view($view)->with('cron_data', $response);
        }
        else
        {
            $view = 'vendor.voyager.retailer.data_notavailable';

            return Voyager::view($view);
        }
        /*$view = 'vendor.voyager.retailer.cron_table_count';

        return Voyager::view($view, compact('cron_data'));*/

        //GET table name as per retailer name
        /*$table = strtolower($retailer_name).'_category';

        if(Schema::hasTable($table))
        {
            $category_count = DB::table($table)
                    ->select('category_name')->count();

            $view = 'vendor.voyager.retailer.category';

            return Voyager::view($view, compact('category_count'));
        }
        else
        {
            exit;
        }*/
    } 


    public function getCategory(Request $request)
    {
        $retailer_name = $_POST['retailer_name'];

        //GET table name as per retailer name
        $table = strtolower($retailer_name).'_category';

        if(Schema::hasTable($table))
        {
            $category_count = DB::table($table)
                    ->select('category_name')->count();

            $view = 'vendor.voyager.retailer.category';

            return Voyager::view($view, compact('category_count'));
        }
        else
        {
            exit;
        }

    } 

    public function getCategoryDetails(Request $request)
    {
        $retailer_name = $_POST['retailer_name'];

        //GET table name as per retailer name
        $table = strtolower($retailer_name).'_category';
        
        $view = 'vendor.voyager.retailer.category_detail';

        return Voyager::view($view, compact('table'));
    }

    public function getProduct(Request $request)
    {
        $retailer_name = $_POST['retailer_name'];

        //GET table name as per retailer name
        $table = strtolower($retailer_name).'_product';

        if(Schema::hasTable($table))
        {
            $product_count = DB::table($table)
                    ->select('name')->count();

            $view = 'vendor.voyager.retailer.product';

            return Voyager::view($view, compact('product_count'));
        }
        else
        {
            exit;
        }
    }

    public function getProductDetails(Request $request)
    {
        $retailer_name = $_POST['retailer_name'];

        //GET table name as per retailer name
        $table = strtolower($retailer_name).'_product';
        
        $view = 'vendor.voyager.retailer.product_detail';

        return Voyager::view($view, compact('table'));
    }

    public function getOffer(Request $request)
    {
        $retailer_name = $_POST['retailer_name'];

        //GET table name as per retailer name
        $table = strtolower($retailer_name).'_offer';

        if(Schema::hasTable($table))
        {
            $offer_count = DB::table($table)
                    ->select('offer_id')->count();

            $view = 'vendor.voyager.retailer.offer';

            return Voyager::view($view, compact('offer_count'));
        }
        else
        {
            exit;
        }

    } 

    public function getOfferDetails(Request $request)
    {
        $retailer_name = $_POST['retailer_name'];

        //GET table name as per retailer name
        $table = strtolower($retailer_name).'_offer';
        
        $view = 'vendor.voyager.retailer.offer_detail';

        return Voyager::view($view, compact('table'));
    }

    public function getPromotion(Request $request)
    {
        $retailer_name = $_POST['retailer_name'];

        //GET table name as per retailer name
        $table = strtolower($retailer_name).'_promotion';

        if(Schema::hasTable($table))
        {
            $promotion_count = DB::table($table)
                    ->select('promotion_id')->count();

            $view = 'vendor.voyager.retailer.promotion';

            return Voyager::view($view, compact('promotion_count'));
        }
        else
        {
            exit;
        }

    } 

    public function getPromotionDetails(Request $request)
    {
        $retailer_name = $_POST['retailer_name'];

        //GET table name as per retailer name
        $table = strtolower($retailer_name).'_promotion';
        
        $view = 'vendor.voyager.retailer.promotion_detail';

        return Voyager::view($view, compact('table'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getXpathDetails(Request $request)
    {
        $id = $request->get('id');

        $view = 'vendor.voyager.retailer.retailer_xpath';

        return Voyager::view($view, compact('id'));
    }

    public function editableData(Request $request)
    {
        $id = $request->post('id');
        $query = $this->xpath::select('*')->where('xpath_id', '=', $id)->first();

        echo json_encode($query);
    }

    public function getXpathAjaxDataTable(Request $request)
    {
        $query = $this->xpath::/*select('xpath_id,field,xpath,retailer_id,retailer_name')
                    ->*/with('retailer')->where('retailer_id', $request->get('id'))->get();
        return Datatables::of($query)
            ->addColumn('action', function($row) {
                return '<button onclick="common.getEditData(this)" data-id="'. $row->xpath_id .'" data-url="'.url('admin/retailer/editableData').'" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#editModal" title="Edit" style="padding:0.05em 4px;"><i class="fa fa-edit"></i></button>';
            })
            ->make(true);
    }
     public function CronTableDetails()
    {
        $view = 'vendor.voyager.retailer.cron_table_details';

        return Voyager::view($view);
        //print_r("expression");exit();
    }

    public function getRetailerAjaxDataTable(Request $request)
    {
        $custom_filter = json_decode($_GET['search']['value']);

        $query = $this->Retailer::select('retailer_name','retailer_id','retailer_cron_schedule');

        return Datatables::of($query)
            ->setRowClass('{{ "custom-active" }}')
            ->filter(function($query) use ($custom_filter) {
                if(is_array($custom_filter)){
                  foreach ($custom_filter as $key) {
                    if($key->value=='true')
                    {
                        $query->where($key->index, '=', true);
                    }
                    else if($key->value=='false')
                    {
                        $query->where($key->index, '=', false);
                    }
                    else
                    {
                        $query->get();
                    }
                  }
                }
                else if(empty($_GET['search']['value'])) {
                  $query->get();
                }
                else { 
                  $query->where('retailer_name', 'like', "%".$_GET['search']['value']."%");
                }
              })
            ->make(true);
    }
    
    public function getProductAjaxDataTable()
    {
        $query = DB::table($_GET['table'])->select('*');
        return Datatables::of($query)
            ->make(true);
    }
    public function getCategoryAjaxDataTable(Request $request)
    {
        $query = DB::table($_GET['table'])->select('*');
        return Datatables::of($query)
            ->make(true);
    } 
    public function getOfferAjaxDataTable()
    {
        $query = DB::table($_GET['table'])->select('*');
        return Datatables::of($query)
            ->make(true);
    }
    public function getPromotionAjaxDataTable()
    {
        $query = DB::table($_GET['table'])->select('*');
        return Datatables::of($query)
            ->make(true);
    }
    
    public function updateRetailerXpath(Request $request)
    {
        $this->xpath::
            where('xpath_id','=',$_POST['id'])
            ->update([
                'last_update_date'=>now(),
                'field' => $_POST['field'],
                'xpath' => $_POST['xpath'],
              ]);

        $response = Response::json('success', 200);
    }
}