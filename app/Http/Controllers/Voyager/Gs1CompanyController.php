<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Schema;
use Yajra\Datatables\Datatables;
use Response;

class Gs1CompanyController extends VoyagerBaseController
{
  public function getCompanyDetails(Request $request)
  {
      $id = $_POST['id'];
      $gs1Company = DB::table('gs1_company')
              ->where('gs1_company_prefix', '=', $id)->first(); 

      $view = 'vendor.voyager.gs1-company.gs1-company-details';

      return Voyager::view($view, compact('gs1Company','id'));
  } 

  public function addMapping(Request $request)
  {
    $manufacturer = explode(",", $request->post('manufacturer'));

    $query = DB::table('gs1_company_manufacturer_mapper');

    foreach ($manufacturer as $key) {
      $query->insert([
        'gs1_company_prefix' => $request->post('id'), 
        'manufacturer' => $key,
        'creation_date' => now(),
        'last_update_date' => now()
      ]);
    }

    $response = Response::json('success', 200);
  }

  public function updateGs1Company(Request $request)
  {
    $response = array();

    DB::table('gs1_company')
        ->where('gs1_company_prefix','=',$_POST['gs1_company_prefix'])
        ->update([
            'last_update_date'=>now(),
            'company_legal_name' => $_POST['company_legal_name'],
            'company_gln' => $_POST['company_gln'],
            'upc_company_prefix' => $_POST['upc_company_prefix'],
            'company_address' => $_POST['company_address'],
            'city' => $_POST['city'],
            'state_or_province' => $_POST['state_or_province'],
            'country' => $_POST['country'],
          ]);

    $tableData = DB::table('gs1_company')
        ->select('creation_date','last_update_date')
        ->where('gs1_company_prefix','=',$_POST['gs1_company_prefix'])
        ->first();

    $response['creation_date'] = $tableData->creation_date;
    $response['last_update_date'] = $tableData->last_update_date;

    echo json_encode($response);
  }

  public function getCompanyManufacturerMap(Request $request)
  {
    $id = $_POST['id'];
    
    $view = 'vendor.voyager.gs1-company.gs1-company-manufacturer-map';

    return Voyager::view($view, compact('id'));
  }

  public function getManufacturer(Request $request)
  {
    $response = array();
    $arr = array();

    $mapping = DB::table('gs1_company_manufacturer_mapper')
              ->select('manufacturer')
              ->where('gs1_company_prefix', '=', $request->get('gs1_company_prefix'))->get();

    foreach ($mapping as $key) {
      $arr[] = $key->manufacturer;
    }

    $query = DB::table('retailer_product')
              ->select('manufacturer')
              ->whereNotIn('manufacturer', $arr)
              ->get();

    /*manufacturers*/
    if($query)
    {
      foreach ($query as $key) {
        $response[] = $key->manufacturer;
      }
    }

    //$data = array_diff($response,$arr);
    echo json_encode($response);
  }

  public function objectToArray($d) 
  {
    if (is_object($d)) {
        $data = array();

        foreach ($d as $key) {
          $data[] = $key;
        }
        return $data;
    }
    else {
        // Return array
        return $d;
    }
  }

  /**
   * Process datatables ajax request.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function getMappingAjaxDataTable(Request $request)
  {
      $query = DB::table('gs1_company_manufacturer_mapper')
                  ->select('*')
                  ->where('gs1_company_prefix','=',$_GET['gs1_company_prefix']);
                  //->Join('retailer_product','gs1_company_manufacturer_mapper.manufacturer','=','retailer_product.retailer_product_id');
      return Datatables::of($query)
          ->addColumn('action', function($row) {
              return '<button onclick="common.appendId(this)" data-id="'. $row->gs1_company_manufacturer_mapper_id .'" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#deleteModal" title="Delete" style="padding:0.01em 5px;"><i class="fa fa-trash"></i></button>';
            })
          ->make(true);
  }

  public function getCompanyListAjaxDataTable(Request $request)
  {
    $query = DB::table('gs1_company')
                  ->select('gs1_company_prefix','company_legal_name')
                  ->orderBy('company_legal_name', 'ASC');
    return Datatables::of($query)
          ->setRowClass('{{ "custom-active" }}')
          ->make(true);
  }

  public function getValidStatus(Request $request)
  {
    $status = DB::table('gs1_company_manufacturer_mapper')
      ->where('gs1_company_manufacturer_mapper_id','=',$request->post('id'));
    
    if($status->first())
    {
      if($status->first()->company_manufacturer_mapping_valid == 1)
      {
        $status->update([
          'last_update_date'=>now(),
          'company_manufacturer_mapping_valid' => '0',
        ]);
      }
      else
      {
        $status->update([
          'last_update_date'=>now(),
          'company_manufacturer_mapping_valid' => '1',
        ]);
      }
      $response = Response::json('success', 200);
    }  
    else
    {
      $response = Response::json('error', 404);
    }
    return $response;

  }

  //***************************************
  //                _____
  //               |  __ \
  //               | |  | |
  //               | |  | |
  //               | |__| |
  //               |_____/
  //
  //         Delete an item BREA(D)
  //
  //****************************************

  public function deleteMapping(Request $request)
  {
    $id = $request->post('id');

    $delete = DB::table('gs1_company_manufacturer_mapper')
                ->where('gs1_company_manufacturer_mapper_id', '=', $id);

    if($delete->first())
    {
      $delete->delete();
      $response = Response::json('success', 200);
    }
    else
    {
      $response = Response::json('error', 404);
    }
    return $response;
  } 
}